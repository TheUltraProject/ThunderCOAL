# // Checksum Definitions for ThunderCOAL

from messageDef import *
from os.path import isfile
from binascii import crc32
from ntpath import basename


# - CRC-32 Dictionaries, to ensure that each file was patched correctly using ThunderCOAL without errors.
# - All dictionaries have subdicts applied to them to account for multiple versions of a singular file.
# - If a file doesn't have multiple versions, the key will be left blank.

# Dictionary for Steam & Greenworks DRM Removal.
DRM_CRC32 = {
"plugins.js":{
    "":"0CA77C2D"},

"ThunderCOAL.js":{
    "209":"E6A56FA0",
    "208":"E98B5CB5",
    "207":"4D39D505",
    "206":"4C999C3F",
    "205":"C316AFF7",
    "204":"E97FAE04",
    "203":"118E8A5B",
    "202":"F1BC6789",
    "201":"8852CED2",
    "200":"AD97A2BE"}
}



# The CRC-32 Calculator that is responsible for the CRC-32 verification.
def CHK_CRC32(f):
    
    f.seek(0)
    crc32Chk = (crc32(f.read()) & 0xFFFFFFFF)
    return ("%08X" % crc32Chk)

# The File Verifier; verifies if a file exists and compares its' CRC-32.
def CHK_VerifyFile(f, sub):
    with open(f, 'rb') as fVerify:
        if isfile(f) != True:
            MSG_MessageHandler("VerifyFile0")
            return False
        
        MSG_MessageHandler("VerifyFile0", 1)
        
        if CHK_CRC32(fVerify) != DRM_CRC32[basename(f)][sub]:
            MSG_MessageHandler("VerifyFile1")
            return False
        
        MSG_MessageHandler("VerifyFile1", 1)
        
        fVerify.close()
    
    return True