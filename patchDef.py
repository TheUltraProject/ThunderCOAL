# // Code Patch Definitions for ThunderCOAL (used for Steam & Greenworks DRM Removal).


from steamDef import *
from messageDef import *
from miscDef import MISC_StringFormat



# - DRM_CODE(XXX) contains every line of code that contains Steam & Greenworks DRM, for each known version of TCOAL.
# - Format uses the line of code as the key, and the amount of times to remove code after that line as the value.
# - Hopefully this format of code removal can be used for other projects, as it's very versatile.

# Order of steps done to remove DRM:
# 1: Completely remove Steam initialization function.
# 2: Nullify debugging function that awards a Steam achievement when the string "achv" is typed in the command prompt.
# 3: Remove "steam" value in App.user() dictionary, which normally returns information about the current Steam users.
# 4: Nullify switch-case debugging function that clears all Steam achievements when Ctrl + Alt + Delete is pressed.
# 5: Remove the entire Steam() function and other related functions, disabling Greenworks in the process and removing all DRM.
# 6 (via Plugin): Replace all mentions of the Steam.currentLanguage() variable to a string specified by STEAM_GetLang(), allowing the dialogue to work properly.



# {v2.0.9}
DRM_209 = {
22:3,
85:0,
348:0,
471:0,
998:103
}

# {v2.0.8 / v2.0.7}
DRM_207 = {
22:3,
85:0,
323:0,
446:0,
965:103
}

# {v2.0.6 / v2.0.5 / v2.0.4 / v2.0.3 / v2.0.2 / v2.0.1 / v2.0.0}
DRM_200 = {
22:3,
85:0,
323:0,
446:0,
954:103
}



# Fetch the corresponding patch based on which string was returned (string should be grabbed from the internal code's VERSION constant).
def DRM_GetPatch(ver):
    match ver:
        case "2.0.9":
            return DRM_209
        case "2.0.8" | "2.0.7":
            return DRM_207
        case "2.0.6" | "2.0.5" | "2.0.4" | "2.0.3" | "2.0.2" | "2.0.1" | "2.0.0":
            return DRM_200
        case _:
            return MSG_MessageHandler("GetVersionDRM")



# The Code Patcher; removes Steam & Greenworks DRM Lines, allowing the game to run without being connected to Steam.
def DRM_CodePatch(f, ver):

    PatchVersion = DRM_GetPatch(ver)

    f.seek(0)
    fList = f.readlines()
    
    i0 = 0
    PatchSubIndex = 0
    PatchDictKey = list(PatchVersion.keys())
    PatchDictValue = list(PatchVersion.values())

    while (i0 != len(PatchVersion)):
        i1 = 0
        if i0 != 0:
            PatchSubIndex += PatchDictValue[i0 - 1]
        
        PatchIndex = ((PatchDictKey[i0]) - PatchSubIndex - (i0 if i0 != 0 else i0 + 1))
        
        del fList[PatchIndex]
        
        if PatchDictValue[i0] != 0:
            while(True):
                i1 += 1
                if i1 == PatchDictValue[i0]:
                    break
                del fList[PatchIndex]
        
        i0 += 1
    
    # Output is a variable to make modifications easier.
    fOutput = "".join(fList)
    # As an example, I have provided a subpatch that changes the Steam.currentLanguage() variable to "english" to allow the dialogue to function.
    fPatch = fOutput.replace("Steam.currentLanguage()", MISC_StringFormat(STEAM_GetLang("en")))

    return fPatch