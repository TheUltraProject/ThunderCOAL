# // Steam & Greenworks Definitions for ThunderCOAL

from messageDef import *



# List of every language available on Steam, in Steam's API format.
# This is all publicly available on https://partner.steamgames.com/doc/store/localization/languages, so it shouldn't be a problem to host.
# Format uses the BCP-47 Code, followed by the corresponding Steam API Language Code.
STEAM_LangDict = {
    "ar":"arabic",
    "bg":"bulgarian",
    "zh-CN":"schinese",
    "zh-TW":"tchinese",
    "cs":"czech",
    "da":"danish",
    "nl":"dutch",
    "en":"english",
    "fi":"finnish",
    "fr":"french",
    "de":"german",
    "el":"greek",
    "hu":"hungarian",
    "id":"indonesian",
    "it":"italian",
    "ja":"japanese",
    "ko":"koreana",
    "no":"norwegian",
    "pl":"polish",
    "pt":"portuguese",
    "pt-BR":"brazilian",
    "ro":"romanian",
    "ru":"russian",
    "es":"spanish",
    "es-419":"latam",
    "sv":"swedish",
    "th":"thai",
    "tr":"turkish",
    "uk":"ukrainian",
    "vn":"vietnamese"
}

def STEAM_GetLang(string):
    if string not in STEAM_LangDict.keys():
        MSG_MessageHandler("SteamGetLang")
    return STEAM_LangDict[string]