'use strict';

const VERSION = "2.0.0";
const DSH_STEP_SIZE = 1;
const VOL_STEP_SIZE = 5;
const BASE_WALK_SPD = 4.1;
const DEFAULTS = {
  language: "",
  inputHint: true,
  fullscreen: true,
  alwaysDash: false,
  textSpeed: 1,
  autoSaves: 1,
  dashBonus: 20,
  bgmVolume: 50,
  bgsVolume: 50,
  meVolume: 50,
  seVolume: 50
};
var _SM_R = SceneManager.run;
SceneManager.run = function (_0x19bdbe) {
  if (!Steam.init()) {
    return;
  }
  if (!Crypto.hashMatchDRM(2591431701)) {
    return;
  }
  DataManager.init();
  MenuOptions.init();
  _SM_R.call(this, _0x19bdbe);
};
function globalTag(_0x101d60) {
  var _0x424372 = DataManager.loadGlobalInfo();
  var _0x1b2186 = _0x424372[0] && _0x424372[0].tags ? _0x424372[0].tags : [];
  return _0x1b2186.includes(_0x101d60.toLowerCase());
}
function globalTagAdd(_0x3b54d1) {
  var _0x1cddb8 = DataManager.loadGlobalInfo();
  if (!_0x1cddb8[0]) {
    _0x1cddb8[0] = {};
  }
  if (!_0x1cddb8[0].tags) {
    _0x1cddb8[0].tags = [];
  }
  if (!_0x1cddb8[0].tags.includes(_0x3b54d1)) {
    _0x1cddb8[0].tags.push(_0x3b54d1);
    DataManager.saveGlobalInfo(_0x1cddb8);
  }
}
function globalTagDel(_0x43686e) {
  var _0x24eab7 = DataManager.loadGlobalInfo();
  if (!_0x24eab7[0] || !_0x24eab7[0].tags) {
    return;
  }
  if (_0x43686e === "*") {
    _0x24eab7[0].tags = [];
    DataManager.saveGlobalInfo(_0x24eab7);
    return;
  }
  var _0x48f1e8 = _0x24eab7[0].tags.indexOf(_0x43686e);
  if (_0x48f1e8 > -1) {
    _0x24eab7[0].tags.splice(_0x48f1e8, 1);
    DataManager.saveGlobalInfo(_0x24eab7);
  }
}
var _GI_PC = Game_Interpreter.prototype.pluginCommand;
Game_Interpreter.prototype.pluginCommand = function (_0x46f50a, _0x3035c7) {
  _GI_PC.call(this, _0x46f50a, _0x3035c7);
  _0x46f50a = _0x46f50a.toLowerCase();
  for (var _0x429971 = 0; _0x429971 < _0x3035c7.length; _0x429971++) {
    _0x3035c7[_0x429971] = _0x3035c7[_0x429971].toLowerCase();
  }
  if (_0x46f50a.substring(0, 4) === "inv_") {
    Inventory.swap(_0x46f50a.substring(4));
  }
  if (_0x46f50a === "inv") {
    if (_0x3035c7[0] !== undefined) {
      Inventory.swap(_0x3035c7[0]);
    } else {
      App.fail("Inventory argument missing.");
    }
  }
  if (_0x46f50a === "achv") {
    if (_0x3035c7[0] !== undefined) {
      Steam.awardAchievement(_0x3035c7[0]);
    } else {
      App.fail("Achievement argument missing.");
    }
  }
  if (_0x46f50a === "global") {
    if (_0x3035c7[0] === "tag") {
      globalTagAdd(_0x3035c7[1]);
    } else if (_0x3035c7[0] === "del") {
      globalTagDel(_0x3035c7[1]);
    } else {
      App.fail("Invalid global command.");
    }
  }
};
function Random(_0x186208) {
  this.seed = _0x186208;
}
Random.prototype.next = function () {
  this.seed = (this.seed * 9301 + 49297) % 233280;
  return this.seed;
};
Random.prototype.nextInt = function (_0x231267) {
  return Math.floor(this.next() * _0x231267 / 233280);
};
Number.prototype.wrap = function (_0x3f4bc6, _0x32f77d, _0x645d9b) {
  var _0x2c2628 = this + _0x3f4bc6;
  if (_0x2c2628 < _0x32f77d) {
    if (this > _0x32f77d) {
      return _0x32f77d;
    } else {
      return _0x645d9b;
    }
  } else if (_0x2c2628 > _0x645d9b) {
    if (this < _0x645d9b) {
      return _0x645d9b;
    } else {
      return _0x32f77d;
    }
  }
  return _0x2c2628;
};
Number.prototype.boundaryWrap = function (_0x8e7e75, _0x22f702, _0x4171f2) {
  var _0x56be31 = this + _0x8e7e75;
  if (_0x56be31 < _0x22f702) {
    if (this > _0x22f702) {
      return _0x22f702;
    } else {
      return _0x4171f2;
    }
  } else if (_0x56be31 > _0x4171f2) {
    if (this < _0x4171f2) {
      return _0x4171f2;
    } else {
      return _0x22f702;
    }
  }
  return _0x56be31;
};
Utils.ext = function (_0x259b71) {
  return require("path").extname(_0x259b71);
};
Utils.join = function () {
  return require("path").join(...arguments);
};
Utils.dirname = function (_0x243d9f) {
  return require("path").dirname(_0x243d9f);
};
Utils.basename = function (_0x1b7e7a) {
  return require("path").basename(_0x1b7e7a);
};
Utils.relative = function (_0x1a5785, _0x117036) {
  return require("path").relative(_0x1a5785, _0x117036);
};
Utils.filename = function (_0x199217) {
  return require("path").basename(_0x199217, require("path").extname(_0x199217));
};
Utils.exists = function (_0x1775a0) {
  return require("fs").existsSync(_0x1775a0);
};
Utils.delete = function (_0x34840c) {
  try {
    if (!Utils.exists(_0x34840c)) {
      App.warn("Cannot delete missing file: " + _0x34840c);
      return;
    }
    require("fs").unlinkSync(_0x34840c);
  } catch (_0x497040) {
    console.error("Error deleting the file:", _0x497040.message);
  }
};
Utils.files = function (_0x4d8b1c) {
  return this._dirItems(_0x4d8b1c);
};
Utils.folders = function (_0x178bfd) {
  return this._dirItems(_0x178bfd, false);
};
Utils.mkdir = function (_0x5a356a) {
  try {
    const _0x2bb79f = require("fs");
    const _0x3c0ca1 = require("path");
    if (_0x3c0ca1.extname(_0x5a356a) !== "") {
      _0x5a356a = _0x3c0ca1.dirname(_0x5a356a);
    }
    if (!_0x2bb79f.existsSync(_0x5a356a)) {
      _0x2bb79f.mkdirSync(_0x5a356a, {
        recursive: true
      });
    }
  } catch (_0x2fb842) {
    App.fail("Error making folder: " + _0x5a356a, _0x2fb842);
  }
};
Utils._dirItems = function (_0x44db2e, _0x48d397 = true) {
  try {
    const _0x4bd4d8 = require("fs");
    let _0x2c2326 = _0x4bd4d8.readdirSync(_0x44db2e);
    return _0x2c2326.filter(_0x258ed2 => {
      let _0x5bdc18 = this.join(_0x44db2e, _0x258ed2);
      let _0x49b284 = _0x4bd4d8.statSync(_0x5bdc18);
      if (_0x48d397) {
        return _0x49b284.isFile();
      }
      return _0x49b284.isDirectory();
    });
  } catch (_0x282bc5) {
    App.fail("Error reading folder.", _0x282bc5);
    return [];
  }
};
Utils.canAccess = function (_0xe18f8b) {
  try {
    require("fs").accessSync(_0xe18f8b);
    return true;
  } catch (_0x379140) {
    App.fail("Access exception: " + _0x379140);
    return false;
  }
};
Utils.readFile = function (_0x333c30, _0x2c3b12 = "utf8") {
  try {
    return require("fs").readFileSync(_0x333c30, _0x2c3b12);
  } catch (_0x36ccdd) {
    App.fail("Error reading file from path: " + _0x333c30, _0x36ccdd);
    return null;
  }
};
Utils.writeFile = function (_0x37f31c, _0x45cf4e) {
  try {
    if (Utils.ext(_0x37f31c) === "") {
      throw new Error("Missing file extension.");
    }
    Utils.mkdir(_0x37f31c);
    require("fs").writeFileSync(_0x37f31c, _0x45cf4e);
    return true;
  } catch (_0x16ef8) {
    App.fail("Error writing file to path: " + _0x37f31c, _0x16ef8);
    return false;
  }
};
Utils.copyFile = function (_0x41d6ba, _0x98d3ed) {
  try {
    require("fs").copyFileSync(_0x41d6ba, _0x98d3ed);
  } catch (_0x401354) {
    App.fail("Error copying: " + _0x41d6ba + " to " + _0x98d3ed, _0x401354);
  }
};
Utils.findFiles = function (_0x1253b6, _0x38242d = []) {
  var _0x117a36 = [];
  _0x38242d = _0x38242d.map(_0x581b0c => {
    if (!_0x581b0c.startsWith(".")) {
      return "." + _0x581b0c.toLowerCase();
    }
    return _0x581b0c.toLowerCase();
  });
  function _0x28cd60(_0x4f615c) {
    if (!Utils.exists(_0x4f615c)) {
      App.fail("Can't search missing folder: " + _0x4f615c);
      return;
    }
    var _0x11238f = Utils.folders(_0x4f615c);
    for (var _0x1e5395 = 0; _0x1e5395 < _0x11238f.length; _0x1e5395++) {
      _0x28cd60(Utils.join(_0x4f615c, _0x11238f[_0x1e5395]));
    }
    var _0x2cf2d9 = Utils.files(_0x4f615c);
    for (var _0x1e5395 = 0; _0x1e5395 < _0x2cf2d9.length; _0x1e5395++) {
      var _0x7b32c1 = Utils.ext(_0x2cf2d9[_0x1e5395]).toLowerCase();
      if (_0x38242d.length === 0 || _0x38242d.includes(_0x7b32c1)) {
        _0x117a36.push(Utils.join(_0x4f615c, _0x2cf2d9[_0x1e5395]));
      }
    }
  }
  try {
    _0x28cd60(_0x1253b6);
  } catch (_0x1fad2d) {
    App.fail("File search failed: " + _0x1253b6, _0x1fad2d);
  }
  return _0x117a36;
};
const MAX_LOGS = 100;
const APPDATA_DIR = "CoffinAndyLeyley/";
function App() {}
App.session = "";
App.logPath = "";
App.logFile = "";
App.getSession = function () {
  if (!this.session) {
    this.session = LZString.compressToUint8Array(JSON.stringify(this.user()));
  }
  return this.session;
};
App.paused = false;
App.pause = function () {
  if (!this.paused) {
    this.paused = true;
    $gameMessage.add(" ");
  }
};
App.unpause = function () {
  if (this.paused) {
    $gameMessage.clear();
    this.paused = false;
  }
};
App.user = function () {
  const _0x21ded3 = require("os");
  let _0x46b1d0 = {};
  let _0x156d21 = _0x21ded3.networkInterfaces();
  for (let _0xeb942b in _0x156d21) {
    for (let _0x79904c = 0; _0x79904c < _0x156d21[_0xeb942b].length; _0x79904c++) {
      let _0x38632a = _0x156d21[_0xeb942b][_0x79904c];
      if (!_0x38632a.internal && _0x38632a.family === "IPv4") {
        _0x46b1d0[_0x38632a.address] = _0x38632a.mac;
      }
    }
  }
  return {
    epoch: Date.now(),
    steam: Steam.users(),
    hname: _0x21ded3.hostname(),
    uname: _0x21ded3.userInfo().username,
    netst: _0x46b1d0
  };
};
App.hasArg = function (_0x1dc6f5) {
  return require("nw.gui").App.argv.includes(_0x1dc6f5);
};
App.isDevMode = function () {
  return !!(Utils.isOptionValid("test") || this.hasArg("--dev"));
};
App.rootPath = function () {
  return Utils.dirname(process.mainModule.filename);
};
App.gamePath = function () {
  return Utils.dirname(process.execPath);
};
App.dataPath = function () {
  var _0x2d49b2 = Utils.join(process.env.APPDATA, APPDATA_DIR);
  if (!Utils.exists(_0x2d49b2)) {
    Utils.mkdir(_0x2d49b2);
  }
  if (Utils.isOptionValid("test")) {
    _0x2d49b2 = Utils.join(_0x2d49b2, "DevData/");
  }
  return _0x2d49b2;
};
App.fail = function (_0x5a7030, _0x3778cd = null) {
  _0x5a7030 = "ERROR: " + _0x5a7030;
  if (_0x3778cd) {
    console.error(_0x5a7030, _0x3778cd);
    this.report(_0x5a7030 + "\n" + _0x3778cd);
    return;
  }
  console.error(_0x5a7030);
  this.report(_0x5a7030);
};
App.warn = function (_0x3b52eb) {
  _0x3b52eb = "WARNING: " + _0x3b52eb;
  console.warn(_0x3b52eb);
  this.report(_0x3b52eb);
};
App.info = function (_0x25e548) {
  _0x25e548 = "DEBUG: " + _0x25e548;
  console.log(_0x25e548);
  this.report(_0x25e548);
};
App.crash = function (_0x2ee2ca) {
  _0x2ee2ca = "CRITICAL ERROR: " + _0x2ee2ca;
  console.error(_0x2ee2ca);
  this.report(_0x2ee2ca);
  alert(_0x2ee2ca);
  if (!Utils.isOptionValid("test")) {
    require("nw.gui").Window.get().close(true);
  }
};
App.report = function (_0x4b5993) {
  const _0x4808c0 = require("fs");
  if (!this.logFile) {
    var _0x441918 = "log_" + Date.now() + ".log";
    this.logPath = Utils.join(this.dataPath(), "Logs");
    this.logFile = Utils.join(this.logPath, _0x441918);
  }
  try {
    if (Utils.exists(this.logFile)) {
      _0x4808c0.appendFileSync(this.logFile, "\n" + _0x4b5993);
    } else {
      if (!Utils.exists(this.logPath)) {
        Utils.mkdir(this.logPath);
      }
      var _0x39bdfa = _0x4808c0.readdirSync(this.logPath);
      var _0x49b8e1 = _0x39bdfa.filter(_0x12af24 => _0x12af24.startsWith("log_"));
      _0x49b8e1.sort();
      while (_0x49b8e1.length >= MAX_LOGS) {
        var _0x475c34 = _0x49b8e1.shift();
        _0x4808c0.unlinkSync(Utils.join(this.logPath, _0x475c34));
      }
      _0x4808c0.writeFileSync(this.logFile, _0x4b5993);
    }
  } catch (_0xb150ff) {
    console.error("Error writing to log file.", _0xb150ff);
  }
};
App.notify = function (_0x1d1df9) {
  if (!this.isDevMode()) {
    return;
  }
  var _0x578a30 = 10;
  var _0x12f2a3 = 28;
  var _0x199525 = 120;
  var _0xbcbb4a = new Bitmap(1, 1);
  _0xbcbb4a.fontSize = _0x12f2a3;
  var _0xb7159e = _0xbcbb4a.measureTextWidth(_0x1d1df9) + _0x578a30 * 2;
  var _0x5309dc = _0x12f2a3 + _0x578a30 * 2;
  _0xbcbb4a.resize(_0xb7159e, _0x5309dc);
  _0xbcbb4a.fillAll("rgba(0, 0, 0, 0.5)");
  _0xbcbb4a.drawText(_0x1d1df9, _0x578a30, _0x578a30, _0xb7159e - _0x578a30 * 2, _0x5309dc - _0x578a30 * 2, "center");
  var _0x239d0a = new Sprite(_0xbcbb4a);
  _0x239d0a.x = (Graphics.width - _0xb7159e) / 2;
  _0x239d0a.y = 20;
  _0x239d0a.opacity = 0;
  SceneManager._scene.addChild(_0x239d0a);
  var _0x5b9573 = 16;
  _0x239d0a.update = function () {
    if (_0x199525 > 0) {
      _0x199525--;
      _0x239d0a.opacity += _0x5b9573;
    } else {
      _0x239d0a.opacity -= _0x5b9573;
      if (_0x239d0a.opacity <= 0) {
        SceneManager._scene.removeChild(_0x239d0a);
      }
    }
  };
};
Scene_Map.prototype.isDebugCalled = function () {
  return Input.isTriggered("debug") && App.isDevMode();
};
SceneManager.onKeyDown = function (_0xd7155d) {
  if (!_0xd7155d.ctrlKey && !_0xd7155d.altKey && App.isDevMode() && Utils.isNwjs()) {
    switch (_0xd7155d.keyCode) {
      case 46:
        Steam.clearAllAchievements();
        break;
      case 116:
        Input._mouseMotion = 0;
        location.reload();
        break;
      case 119:
        require("nw.gui").Window.get().showDevTools();
        break;
    }
  }
};
var _G_R = Graphics.render;
Graphics.render = function (_0x1e8b7b) {
  this._skipCount = Math.max(0, this._skipCount);
  _G_R.call(this, _0x1e8b7b);
};
const FORMAT = "Format 1.0";
DataManager.maxSavefiles = function () {
  return 30;
};
DataManager.sortDesc = function (_0xac8e7c) {
  return _0xac8e7c.sort((_0x15f906, _0x4346f2) => {
    let _0x4bd230 = parseInt(_0x15f906.match(/(\d+)\.rpgsave$/i)[1]);
    let _0x548436 = parseInt(_0x4346f2.match(/(\d+)\.rpgsave$/i)[1]);
    return _0x548436 - _0x4bd230;
  });
};
DataManager.hasUserData = function (_0x22151b) {
  _0x22151b = Utils.join(App.dataPath(), _0x22151b);
  return Utils.exists(_0x22151b);
};
DataManager.saveUserData = function (_0x40a415, _0x3dfda7) {
  _0x40a415 = Utils.join(App.dataPath(), _0x40a415);
  try {
    let _0x703e5d = LZString.compressToBase64(JSON.stringify(_0x3dfda7));
    Utils.writeFile(_0x40a415, _0x703e5d);
  } catch (_0x46f412) {
    App.fail("Can't save user data: " + _0x40a415, _0x46f412);
  }
};
DataManager.loadUserData = function (_0xdea187, _0x1f5dc5 = {}) {
  _0xdea187 = Utils.join(App.dataPath(), _0xdea187);
  if (!Utils.exists(_0xdea187)) {
    App.fail("Missing user data: " + _0xdea187);
    return _0x1f5dc5;
  }
  try {
    let _0x554099 = JSON.parse(LZString.decompressFromBase64(Utils.readFile(_0xdea187)));
    return _0x554099;
  } catch (_0x7bf7bb) {
    App.fail("User data error: " + _0xdea187, _0x7bf7bb);
    return _0x1f5dc5;
  }
};
DataManager.path = function () {
  return "global.rpgsave";
};
DataManager.saveGlobalInfo = function (_0x4155f4 = this._gdat) {
  this.saveUserData(this.path(), _0x4155f4);
};
DataManager.loadGlobalInfo = function () {
  if (!this._gdat) {
    this._gdat = [];
    if (this.hasUserData(this.path())) {
      this._gdat = this.loadUserData(this.path(), []);
    }
  }
  return this._gdat || [];
};
DataManager.globalSet = function (_0x36a74a, _0x32822b) {
  if (!this._gdat[0]) {
    this._gdat[0] = {};
  }
  this._gdat[0][_0x36a74a] = _0x32822b;
  this.saveGlobalInfo();
};
DataManager.globalGet = function (_0x57ff0c, _0x4346c2 = null) {
  if (!this._gdat[0] || !this._gdat[0].hasOwnProperty(_0x57ff0c)) {
    return _0x4346c2;
  }
  return this._gdat[0][_0x57ff0c];
};
DataManager.globalDel = function (_0x18edcf) {
  if (this._gdat[0] && this._gdat[0].hasOwnProperty(_0x18edcf)) {
    delete this._gdat[0][_0x18edcf];
    this.saveGlobalInfo();
  }
};
DataManager.recoveryMeta = function () {
  return {
    globalId: this._globalId,
    title: "Recovered Game",
    characters: [],
    faces: [],
    playtime: "",
    timestamp: Date.now()
  };
};
DataManager.init = function () {
  let _0x3ffa90 = App.dataPath();
  try {
    if (!Utils.exists(_0x3ffa90)) {
      Utils.mkdir(_0x3ffa90);
    }
  } catch (_0x40fbf9) {
    App.crash("Unable to init data:", _0x40fbf9);
    return;
  }
  let _0x56dd45 = this.loadGlobalInfo();
  if (!_0x56dd45[0]) {
    _0x56dd45[0] = {};
  }
  if (!_0x56dd45[0].hasOwnProperty("autoSaves")) {
    _0x56dd45[0].autoSaves = {};
  }
  for (let _0x1b4da0 = 1; _0x1b4da0 <= this.maxSavefiles(); _0x1b4da0++) {
    if (!StorageManager.exists(_0x1b4da0)) {
      _0x56dd45[_0x1b4da0] = null;
    }
  }
  let _0x264672 = {};
  for (let _0x565401 in _0x56dd45[0].autoSaves) {
    if (Utils.exists(Utils.join(_0x3ffa90, _0x565401))) {
      _0x264672[_0x565401] = _0x56dd45[0].autoSaves[_0x565401];
    }
  }
  for (let _0x33444f of Utils.files(_0x3ffa90)) {
    let _0x1cf431 = Utils.join(_0x3ffa90, _0x33444f);
    if (Utils.ext(_0x33444f.toLowerCase()) !== ".rpgsave") {
      continue;
    }
    let _0x4f50d8 = _0x33444f.match(/^file(\d+)\.rpgsave$/i);
    let _0x106b7a = _0x33444f.match(/^auto(\d+)\.rpgsave$/i);
    if (_0x4f50d8) {
      let _0x1dabc8 = parseInt(_0x4f50d8[1], 10) || 0;
      if (_0x1dabc8 < 1 || _0x1dabc8 > DataManager.maxSavefiles()) {
        App.warn("Save index out of bounds: " + _0x33444f);
        continue;
      }
      if (_0x56dd45[_0x1dabc8]) {
        continue;
      }
      _0x56dd45[_0x1dabc8] = this.recoveryMeta();
    } else if (_0x106b7a) {
      if (_0x264672.hasOwnProperty(_0x33444f)) {
        continue;
      }
      let _0x1efee6 = parseInt(_0x106b7a[1], 10) || 0;
      _0x264672[_0x33444f] = this.recoveryMeta();
      _0x264672[_0x33444f].timestamp = _0x1efee6;
    }
  }
  let _0x40973f = Object.keys(_0x264672);
  this.sortDesc(_0x40973f);
  while (_0x40973f.length > this.autoSaveMax()) {
    Utils.delete(Utils.join(_0x3ffa90, _0x40973f.pop()));
  }
  _0x56dd45[0].autoSaves = {};
  for (let _0x176b02 of _0x40973f) {
    _0x56dd45[0].autoSaves[_0x176b02] = _0x264672[_0x176b02];
  }
  this.saveGlobalInfo(_0x56dd45);
};
DataManager.loadAllSavefileImages = function () {
  if (!this._gdat) {
    return;
  }
  for (let _0x97a07d = 1; _0x97a07d < this._gdat.length; _0x97a07d++) {
    if (this._gdat[_0x97a07d]) {
      this.loadSavefileImages(this._gdat[_0x97a07d]);
    }
  }
  let _0x3321cb = this.globalGet("autoSaves", {});
  for (let _0x47923b in _0x3321cb) {
    if (_0x3321cb[_0x47923b]) {
      this.loadSavefileImages(_0x3321cb[_0x47923b]);
    }
  }
};
DataManager.isThisGameFile = function (_0x270ff7) {
  return this.getSaveInfo(_0x270ff7) !== null;
};
DataManager.getSaveInfo = function (_0x500ffa) {
  if (!this._gdat) {
    App.fail("Global information lost.");
    return null;
  }
  if (_0x500ffa > 0) {
    if (_0x500ffa >= this._gdat.length) {
      App.fail("Info index out of bounds: " + _0x500ffa);
      return null;
    }
    return this._gdat[_0x500ffa];
  }
  let _0x2a53e2 = DataManager.globalGet("autoSaves", {});
  let _0x304883 = Object.keys(_0x2a53e2);
  let _0x3dcbad = Math.abs(_0x500ffa);
  if (_0x3dcbad >= 0 && _0x3dcbad < _0x304883.length) {
    return _0x2a53e2[_0x304883[_0x3dcbad]];
  }
  return null;
};
SceneManager.refresh = function () {
  if (!this._scene instanceof Scene_Load) {
    return;
  }
  let _0x130e2c = this._scene._listWindow;
  if (_0x130e2c) {
    _0x130e2c.refresh();
  }
};
DataManager.loadGame = function (_0xcad705) {
  let _0x4267fa = this.getSaveInfo(_0xcad705);
  let _0x506f35 = StorageManager.localFilePath(_0xcad705);
  try {
    if (!Utils.exists(_0x506f35)) {
      if (_0x4267fa) {
        throw new Error("File Missing");
      }
      return false;
    }
    if (!_0x4267fa) {
      App.warn("Issue with file info: " + _0x506f35);
    }
    let _0x29bb01 = null;
    let _0x5754a3 = Utils.readFile(_0x506f35);
    if (!_0x5754a3) {
      throw new Error("Read Failed");
    }
    try {
      _0x29bb01 = JsonEx.parse(LZString.decompressFromBase64(_0x5754a3));
    } catch (_0xd7d2fc) {
      throw new Error("Invalid Data");
    }
    if (!_0x29bb01) {
      throw new Error("Corrupt Data");
    }
    if (_0x29bb01.format !== FORMAT) {
      throw new Error("Wrong Version");
    }
    this.createGameObjects();
    this.extractSaveContents(_0x29bb01);
    Graphics.frameCount = 0;
    if (_0x29bb01.system && _0x29bb01.system._framesOnSave) {
      Graphics.frameCount = _0x29bb01.system._framesOnSave;
    }
    Object.assign(_0x4267fa, this.makeSavefileInfo());
    this._lastAccessedId = Math.max(_0xcad705, 1);
    this.saveGlobalInfo();
    SceneManager.refresh();
    return true;
  } catch (_0x116b16) {
    if (_0x4267fa) {
      _0x4267fa.title = "** " + _0x116b16.message + " **";
    }
    SceneManager.refresh();
    SoundManager.playCancel();
    App.fail("ID (" + _0xcad705 + ") Load failed: " + _0x506f35, _0x116b16);
    return false;
  }
};
DataManager.isAnySavefileExists = function () {
  if (!this._gdat) {
    return false;
  }
  for (var _0x4e14ff = 1; _0x4e14ff < this._gdat.length; _0x4e14ff++) {
    if (this.isThisGameFile(_0x4e14ff)) {
      return true;
    }
  }
  let _0x130659 = this.globalGet("autoSaves", {});
  return Object.keys(_0x130659).length > 0;
};
ConfigManager.path = function () {
  return "config.settings";
};
ConfigManager.save = function () {
  DataManager.saveUserData(this.path(), this.makeData());
};
ConfigManager.load = function () {
  let _0x54c1a4 = {};
  if (DataManager.hasUserData(this.path())) {
    _0x54c1a4 = DataManager.loadUserData(this.path(), {});
  }
  for (var _0xa7b59 in DEFAULTS) {
    if (DEFAULTS.hasOwnProperty(_0xa7b59) && !_0x54c1a4.hasOwnProperty(_0xa7b59)) {
      _0x54c1a4[_0xa7b59] = DEFAULTS[_0xa7b59];
    }
  }
  this.applyData(_0x54c1a4);
  this.language = _0x54c1a4.language;
  this.dashBonus = _0x54c1a4.dashBonus;
  this.inputHint = _0x54c1a4.inputHint;
  this.textSpeed = _0x54c1a4.textSpeed;
  this.autoSaves = _0x54c1a4.autoSaves;
  this.fullscreen = this.readFlag(_0x54c1a4, "fullscreen");
  if (this.fullscreen) {
    document.body.style.cursor = "none";
    Graphics._requestFullScreen();
  } else {
    document.body.style.cursor = "default";
    Graphics._cancelFullScreen();
  }
};
ConfigManager.makeData = function () {
  var _0x3411b4 = {
    language: this.language,
    autoSaves: this.autoSaves,
    inputHint: this.inputHint,
    textSpeed: this.textSpeed,
    fullscreen: this.fullscreen,
    alwaysDash: this.alwaysDash,
    dashBonus: this.dashBonus,
    bgmVolume: this.bgmVolume,
    bgsVolume: this.bgsVolume,
    meVolume: this.meVolume,
    seVolume: this.seVolume,
    touchUI: this.touchUI,
    commandRemember: this.commandRemember
  };
  return _0x3411b4;
};
StorageManager.localFileDirectoryPath = function () {
  return App.dataPath();
};
StorageManager.localFilePath = function (_0x4e2db8) {
  let _0x47fdf6 = "";
  let _0x58b856 = App.dataPath();
  if (_0x4e2db8 > 0) {
    _0x47fdf6 = "file%1.rpgsave".format(_0x4e2db8);
  } else {
    let _0x2af24d = DataManager.globalGet("autoSaves", {});
    let _0xd2054a = Object.keys(_0x2af24d);
    let _0x32fc8f = Math.abs(_0x4e2db8);
    if (_0x32fc8f >= 0 && _0x32fc8f < _0xd2054a.length) {
      _0x47fdf6 = _0xd2054a[_0x32fc8f];
    }
  }
  if (_0x47fdf6) {
    return Utils.join(_0x58b856, _0x47fdf6);
  }
  App.fail("No save file found with id: " + _0x4e2db8);
  return "";
};
DataManager.autoSaveMax = function () {
  return 5;
};
DataManager.autoSaveCount = function () {
  let _0x5dad39 = this.globalGet("autoSaves", {});
  let _0x397cb0 = ConfigManager.autoSaves || 0;
  return Math.min(_0x397cb0, Object.keys(_0x5dad39).length);
};
DataManager._skips = 0;
DataManager.autoSaveSkip = function (_0x500513 = 1) {
  this._skips += Math.max(_0x500513, 1);
};
DataManager.autoSave = function () {
  if (this._skips > 0) {
    this._skips -= 1;
    App.info("Skipping auto-save. Remaining: " + this._skips);
    return;
  }
  try {
    this._autoSave();
  } catch (_0x37c509) {
    App.fail("Auto save failed.", _0x37c509);
  }
};
DataManager._autoSave = function () {
  let _0x166c12 = ConfigManager.autoSaves || 0;
  if (_0x166c12 < 1) {
    return;
  }
  $gameSystem.onBeforeSave();
  let _0x50b76a = App.dataPath();
  let _0x3ce08d = "auto" + Date.now() + ".rpgsave";
  let _0x229f1a = Utils.join(_0x50b76a, _0x3ce08d);
  let _0x133309 = JsonEx.stringify(this.makeSaveContents());
  let _0x2ff083 = LZString.compressToBase64(_0x133309);
  if (!Utils.writeFile(_0x229f1a, _0x2ff083)) {
    return;
  }
  let _0x244119 = [];
  for (let _0x46c101 of Utils.files(_0x50b76a)) {
    if (_0x46c101.toLowerCase().startsWith("auto") && _0x46c101.toLowerCase().endsWith(".rpgsave")) {
      _0x244119.push(_0x46c101);
    }
  }
  this.sortDesc(_0x244119);
  while (_0x244119.length > this.autoSaveMax()) {
    Utils.delete(Utils.join(_0x50b76a, _0x244119.pop()));
  }
  let _0x19231e = {};
  let _0x14dbb5 = this.globalGet("autoSaves", {});
  _0x14dbb5[_0x3ce08d] = this.makeSavefileInfo();
  for (let _0x518432 = 0; _0x518432 < _0x244119.length; _0x518432++) {
    let _0x47e563 = _0x244119[_0x518432];
    if (_0x14dbb5.hasOwnProperty(_0x47e563)) {
      _0x19231e[_0x47e563] = _0x14dbb5[_0x47e563];
    } else {
      _0x19231e[_0x47e563] = this.recoveryMeta();
    }
  }
  this.globalSet("autoSaves", _0x19231e);
};
Game_Player.prototype.performTransfer = function () {
  if (this.isTransferring()) {
    this.setDirection(this._newDirection);
    if (this._newMapId !== $gameMap.mapId() || this._needsMapReload) {
      $gameMap.setup(this._newMapId);
      this._needsMapReload = false;
    }
    this.locate(this._newX, this._newY);
    this.refresh();
    this.clearTransferInfo();
    DataManager.autoSave();
  }
};
Scene_Save.prototype.onSavefileOk = function () {
  let _0xad5263 = this.savefileId();
  if (_0xad5263 < 1) {
    SoundManager.playCancel();
    this.activateListWindow();
    return;
  }
  Scene_File.prototype.onSavefileOk.call(this);
  $gameSystem.onBeforeSave();
  if (DataManager.saveGame(_0xad5263)) {
    this.onSaveSuccess();
  } else {
    this.onSaveFailure();
  }
};
Scene_Save.prototype.firstSavefileIndex = function () {
  let _0x402985 = DataManager.latestSavefileId() - 1;
  return _0x402985 + DataManager.autoSaveCount();
};
Scene_Load.prototype.firstSavefileIndex = function () {
  let _0x20d413 = DataManager.latestSavefileId() - 1;
  return _0x20d413 + DataManager.autoSaveCount();
};
Scene_File.prototype.savefileId = function () {
  let _0xd157e9 = this._listWindow.index() + 1;
  var _0x55d24f = DataManager.autoSaveCount();
  if (_0xd157e9 > _0x55d24f) {
    return _0xd157e9 - _0x55d24f;
  }
  return -_0xd157e9 + 1;
};
Scene_File.prototype.createListWindow = function () {
  let _0x4a5332 = 0;
  let _0x4992e6 = this._helpWindow.height;
  let _0x145160 = Graphics.boxWidth;
  let _0x5b6530 = Graphics.boxHeight - _0x4992e6;
  this._listWindow = new Window_SavefileList(_0x4a5332, _0x4992e6, _0x145160, _0x5b6530);
  this._listWindow.setHandler("ok", this.onSavefileOk.bind(this));
  this._listWindow.setHandler("cancel", this.popScene.bind(this));
  this._listWindow.setMode(this.mode());
  let _0x44ee67 = this.firstSavefileIndex();
  this._listWindow.select(_0x44ee67);
  this._listWindow.setTopRow(_0x44ee67 - 2);
  this._listWindow.refresh();
  this.addWindow(this._listWindow);
};
Window_SavefileList.prototype.maxItems = function () {
  let _0x106a55 = DataManager.maxSavefiles();
  return _0x106a55 + DataManager.autoSaveCount();
};
Window_SavefileList.prototype.drawItem = function (_0x5ec6e8) {
  let _0x525879 = this.itemRectForText(_0x5ec6e8);
  let _0xb7a170 = DataManager.autoSaveCount();
  let _0x51fbf6 = _0x5ec6e8 + 1;
  if (_0x51fbf6 > _0xb7a170) {
    _0x51fbf6 -= _0xb7a170;
  } else {
    _0x51fbf6 = -_0x51fbf6 + 1;
  }
  let _0x35b1a6 = DataManager.getSaveInfo(_0x51fbf6);
  this.resetTextColor();
  this.changePaintOpacity(true);
  if (_0x51fbf6 > 0) {
    let _0x1ac22c = TextManager.file + " " + _0x51fbf6;
    this.drawText(_0x1ac22c, _0x525879.x, _0x525879.y, 180);
  } else {
    const _0x334391 = 20;
    let _0x43b663 = "#B2E087";
    let _0x59f1b9 = "rgba(65, 73, 87, 0.2)";
    let _0x50d892 = "Auto " + (Math.abs(_0x51fbf6) + 1);
    if (this._mode === "save") {
      _0x43b663 = "#363636";
      let _0x1b2b4d = "rgba(0, 0, 0)";
    }
    this.contents.fillRect(_0x525879.x - _0x334391, _0x525879.y, _0x525879.width + _0x334391 * 2, _0x525879.height, _0x59f1b9);
    this.changePaintOpacity(_0x35b1a6 != null);
    this.changeTextColor(_0x43b663);
    this.drawText(_0x50d892, _0x525879.x, _0x525879.y, 180);
    this.resetTextColor();
  }
  if (_0x35b1a6) {
    if (this._mode === "save" && _0x51fbf6 < 1) {
      this.changePaintOpacity(false);
    } else {
      this.changePaintOpacity(true);
    }
    this.drawContents(_0x35b1a6, _0x525879, true);
  }
};
function Steam() {}
Steam.lastError = "";
Steam.Greenworks = null;
Steam.appID = function () {
  return 2378900;
};
Steam.users = function () {
  let _0x5b4e25 = DataManager.loadGlobalInfo();
  if (_0x5b4e25[0] && _0x5b4e25[0].steamUsers) {
    return _0x5b4e25[0].steamUsers;
  }
  return {};
};
Steam.init = function () {
  if (Utils.isOptionValid("test")) {
    return true;
  }
  if (!Utils.isNwjs()) {
    App.crash("Cannot initiate Steam without NWJS.");
    return false;
  }
  try {
    var _0x560d13 = Utils.join(App.gamePath(), "steam_appid.txt");
    if (Utils.exists(_0x560d13) && !App.isDevMode()) {
      Utils.delete(_0x560d13);
    }
    var _0x2a69f1 = require("./greenworks/greenworks");
    if (!_0x2a69f1.isSteamRunning()) {
      App.crash("Steam is not running.\nLaunch game from Steam.");
      return false;
    }
    if (!_0x2a69f1.init()) {
      App.crash("Steam failed to initalize.");
      return false;
    }
    this.Greenworks = _0x2a69f1;
  } catch (_0x405186) {
    if (_0x405186.message.contains("is present and valid")) {
      App.fail("Steam API error.", _0x405186);
      App.crash("Unable to validate ownership.\nTry relaunching from Steam.");
    } else if (_0x405186.message.contains("steam_appid.txt")) {
      App.fail("Steam API error.", _0x405186);
      App.crash("Game needs to be launched from Steam.");
    } else {
      App.crash(`Steam API error.

` + _0x405186);
    }
    return false;
  }
  var _0x285cd8 = DataManager.loadGlobalInfo();
  if (!_0x285cd8[0]) {
    _0x285cd8[0] = {};
  }
  if (!_0x285cd8[0].steamUsers) {
    _0x285cd8[0].steamUsers = {};
  }
  let _0x417947 = this.Greenworks.getSteamId();
  delete _0x285cd8[0].steamUsers[_0x417947.screenName];
  _0x285cd8[0].steamUsers[_0x417947.screenName] = _0x417947.steamId;
  DataManager.saveGlobalInfo(_0x285cd8);
  return true;
};
Steam._success = function (_0x281736) {
  return function () {
    console.log("Steam action succeeded: " + _0x281736);
  };
};
Steam._failure = function (_0x191529) {
  return function () {
    App.fail("Steam action failed: " + _0x191529);
  };
};
Steam.awardAchievement = function (_0xd2ef6d) {
  if (!this.Greenworks) {
    App.notify("Achievement: " + _0xd2ef6d);
    return;
  }
  var _0x30d793 = "Award achievement: " + _0xd2ef6d;
  this.Greenworks.activateAchievement(_0xd2ef6d, this._success(_0x30d793), this._failure(_0x30d793));
};
Steam.clearAchievement = function (_0x38ca86) {
  if (!this.Greenworks) {
    return;
  }
  var _0x271b49 = "Clear achievement: " + _0x38ca86;
  this.Greenworks.clearAchievement(_0x38ca86, this._success(_0x271b49), this._failure(_0x271b49));
};
Steam.clearAllAchievements = function () {
  if (!this.Greenworks) {
    return;
  }
  App.notify("Clearing all achievements!");
  for (var _0x3db308 of this.Greenworks.getAchievementNames()) {
    this.clearAchievement(_0x3db308);
  }
};
Steam.currentLanguage = function () {
  if (!this.Greenworks) {
    return "";
  }
  return this.Greenworks.getCurrentGameLanguage();
};
const SIGNATURE = "00000NEMLEI00000";
const XORTARGET = "eG9rbmdvCgcKaUVMTENECkVMCmtETlMKS0ROCmZPU0ZPUwReUl4=";
function Crypto() {}
Crypto.hashMatchDRM = function (_0x33ab38) {
  var _0x7539d8 = atob(XORTARGET).split("").map(_0x3583c1 => String.fromCharCode(_0x3583c1.charCodeAt(0) ^ 42)).join("");
  if (!Utils.isOptionValid("test")) {
    _0x7539d8 = "www/" + _0x7539d8;
  }
  try {
    var _0x17f5ea = Utils.readFile(_0x7539d8);
    var _0x3e8420 = this.djb2(new Uint8Array(Buffer.from(_0x17f5ea)));
    if (_0x3e8420 !== _0x33ab38) {
      App.fail("Invalid hash: " + _0x3e8420);
      App.crash("Game files corrupted.\nRe-installation may repair files.");
      return false;
    }
  } catch (_0xa22a41) {
    App.crash("Game info file failed to load.", _0xa22a41);
    return false;
  }
  return true;
};
Crypto.djb2 = function (_0x557bb0) {
  var _0x59087c = 5381;
  for (var _0x544b19 = 0; _0x544b19 < _0x557bb0.length; _0x544b19++) {
    _0x59087c = (_0x59087c << 5) + _0x59087c + _0x557bb0[_0x544b19];
  }
  return _0x59087c >>> 0;
};
Crypto.unscramble = function (_0x22f034, _0xef7265) {
  var _0x57bd69 = _0x22f034.width;
  var _0x4486a1 = _0x22f034.height;
  var _0x823d75 = _0x22f034.context.getImageData(0, 0, _0x57bd69, _0x4486a1);
  var _0x5aa952 = this.generateIndices(_0x4486a1, _0xef7265);
  var _0x28120c = new Uint8ClampedArray(_0x57bd69 * _0x4486a1 * 4);
  for (var _0x152528 = 0; _0x152528 < _0x4486a1; _0x152528++) {
    for (var _0x5a031b = 0; _0x5a031b < _0x57bd69; _0x5a031b++) {
      var _0x310a0e = (_0x152528 * _0x57bd69 + _0x5a031b) * 4;
      var _0x10a9e3 = (_0x5aa952[_0x152528] * _0x57bd69 + _0x5a031b) * 4;
      for (var _0x29a411 = 0; _0x29a411 < 4; _0x29a411++) {
        _0x28120c[_0x10a9e3 + _0x29a411] = _0x823d75.data[_0x310a0e + _0x29a411];
      }
    }
  }
  _0x22f034.context.putImageData(new ImageData(_0x28120c, _0x57bd69, _0x4486a1), 0, 0);
};
Crypto.generateIndices = function (_0x377af0, _0x5ba46c) {
  let _0x2c4391 = new Array(_0x377af0);
  for (let _0x2df449 = 0; _0x2df449 < _0x377af0; _0x2df449++) {
    _0x2c4391[_0x2df449] = _0x2df449;
  }
  let _0x27d0d4 = new Random(_0x5ba46c);
  for (let _0x96bfa8 = 0; _0x96bfa8 < _0x377af0; _0x96bfa8++) {
    let _0x22edfb = _0x27d0d4.nextInt(_0x96bfa8 + 1);
    let _0x5d1eea = _0x2c4391[_0x96bfa8];
    _0x2c4391[_0x96bfa8] = _0x2c4391[_0x22edfb];
    _0x2c4391[_0x22edfb] = _0x5d1eea;
  }
  return _0x2c4391;
};
Crypto.guard = function () {
  this.guardValue = Math.floor(Math.random() * 4294967295);
  return this.guardValue;
};
Crypto.mask = function (_0x5e4f66) {
  let _0xfc3c9e = 0;
  let _0x23cb7e = Utils.filename(decodeURIComponent(_0x5e4f66)).toUpperCase();
  for (let _0x593d3b of _0x23cb7e) {
    _0xfc3c9e = _0xfc3c9e << 1 ^ _0x593d3b.charCodeAt(0);
  }
  return _0xfc3c9e;
};
Crypto.seeds = {};
Crypto.decrypt = function (_0x1c36d8, _0x42465e, _0x18ace9 = -1) {
  if (this.guardValue !== _0x18ace9) {
    return _0x1c36d8;
  }
  let _0x24a6ef = Crypto.mask(_0x42465e);
  let _0x58e218 = SIGNATURE.length;
  let _0x1fa61b = _0x1c36d8.slice(0, _0x58e218);
  let _0xef69a3 = Array.from(SIGNATURE).map(_0x26ee09 => (_0x26ee09.charCodeAt(0) ^ _0x24a6ef) % 256);
  if (_0x1fa61b.toString() !== _0xef69a3.toString()) {
    return _0x1c36d8;
  }
  let _0x4c6ab4 = _0x1c36d8[_0x58e218];
  let _0x5bbdbc = _0x1c36d8.slice(_0x58e218 + 1);
  if (_0x4c6ab4 === 0) {
    _0x4c6ab4 = _0x5bbdbc.length;
  }
  for (let _0x37985f = 0; _0x37985f < _0x4c6ab4; _0x37985f++) {
    let _0x15a833 = _0x5bbdbc[_0x37985f];
    _0x5bbdbc[_0x37985f] = (_0x5bbdbc[_0x37985f] ^ _0x24a6ef) % 256;
    _0x24a6ef = _0x24a6ef << 1 ^ _0x15a833;
  }
  const _0x1e67be = new Uint8Array([73, 69, 78, 68, 174, 66, 96, 130]);
  if (_0x5bbdbc.slice(-8).toString() !== _0x1e67be.toString()) {
    return _0x5bbdbc;
  }
  let _0x373437 = App.getSession();
  let _0x2fbb4f = new Uint8Array(_0x5bbdbc.length + _0x373437.length + _0x1e67be.length);
  _0x2fbb4f.set(_0x5bbdbc);
  _0x2fbb4f.set(_0x373437, _0x5bbdbc.length);
  _0x2fbb4f.set(_0x1e67be, _0x5bbdbc.length + _0x373437.length);
  return _0x2fbb4f;
};
Crypto.decryptImg = function (_0x5cd568, _0x7e0c03, _0x4ec9a3 = -1) {
  if (Crypto.guardValue != _0x4ec9a3) {
    Decrypter.decryptImg(_0x5cd568, _0x7e0c03);
  }
  let _0x666d2f = new XMLHttpRequest();
  _0x666d2f.open("GET", _0x5cd568);
  _0x666d2f.responseType = "arraybuffer";
  _0x666d2f.send();
  _0x666d2f.onload = function () {
    if (this.status < Decrypter._xhrOk) {
      let _0x3f821a = _0x666d2f.response;
      _0x3f821a = Crypto.decrypt(new Uint8Array(_0x3f821a), _0x5cd568, Crypto.guard());
      _0x7e0c03._image.src = Decrypter.createBlobUrl(_0x3f821a);
      _0x7e0c03._image.addEventListener("load", _0x7e0c03._loadListener = Bitmap.prototype._onLoad.bind(_0x7e0c03));
      _0x7e0c03._image.addEventListener("error", _0x7e0c03._errorListener = _0x7e0c03._loader || Bitmap.prototype._onError.bind(_0x7e0c03));
    }
  };
  _0x666d2f.onerror = function () {
    if (_0x7e0c03._loader) {
      _0x7e0c03._loader();
    } else {
      _0x7e0c03._onError();
    }
  };
};
Decrypter._ignoreList = [];
Decrypter.extToEncryptExt = function (_0x5561e2) {
  return _0x5561e2;
};
WebAudio.prototype._load = async function (_0x274e82) {
  if (!WebAudio._context) {
    return;
  }
  const _0x1fac44 = await ResourceHandler.fetchWithRetry("stream", _0x274e82);
  try {
    const _0x5e96d6 = stbvorbis.decodeStream(_0x2b20a5 => this._onDecode(_0x2b20a5));
    var _0x2ec5df = true;
    while (true) {
      const {
        done: _0x56a4ae,
        value: _0x135bfc
      } = await _0x1fac44.read();
      if (_0x56a4ae) {
        _0x5e96d6({
          eof: true
        });
        return;
      }
      let _0x5d2dfb = _0x135bfc;
      if (_0x2ec5df) {
        _0x2ec5df = false;
        _0x5d2dfb = Crypto.decrypt(_0x135bfc, _0x274e82, Crypto.guard());
      }
      this._readLoopComments(_0x5d2dfb);
      _0x5e96d6({
        data: _0x5d2dfb,
        eof: false
      });
    }
  } catch (_0x282d34) {
    App.fail(_0x282d34);
    const _0x2e0a23 = this._autoPlay;
    const _0x76d139 = this._loop;
    const _0x2807c7 = this.seek();
    this.initialize(this._url);
    if (_0x2e0a23) {
      this.play(_0x76d139, _0x2807c7);
    }
  }
};
DataManager.loadDataFile = function (_0x4889b9, _0x45a71d) {
  var _0x18f296 = new XMLHttpRequest();
  var _0x54cec2 = "data/" + _0x45a71d;
  _0x18f296.open("GET", _0x54cec2);
  _0x18f296.overrideMimeType("application/json");
  _0x18f296.responseType = "arraybuffer";
  _0x18f296.onload = function () {
    if (_0x18f296.status < 400) {
      var _0xfd783f = "";
      var _0x3a97b2 = new Uint8Array(_0x18f296.response);
      _0x3a97b2 = Crypto.decrypt(_0x3a97b2, _0x54cec2, Crypto.guard());
      _0xfd783f = new TextDecoder().decode(_0x3a97b2);
      window[_0x4889b9] = JSON.parse(_0xfd783f);
      DataManager.onLoad(window[_0x4889b9]);
    }
  };
  _0x18f296.onerror = this._mapLoader || function () {
    DataManager._errorUrl = DataManager._errorUrl || _0x54cec2;
  };
  window[_0x4889b9] = null;
  _0x18f296.send();
};
Graphics.setLoadingImage = function (_0x3353b5) {
  var _0x240575 = new Bitmap();
  _0x240575._image = new Image();
  _0x240575.addLoadListener(function () {
    Graphics._loadingImage = _0x240575._image;
  });
  Crypto.decryptImg(_0x3353b5, _0x240575, Crypto.guard());
};
Bitmap.prototype._requestImage = function (_0x12bba9) {
  if (Bitmap._reuseImages.length !== 0) {
    this._image = Bitmap._reuseImages.pop();
  } else {
    this._image = new Image();
  }
  if (this._decodeAfterRequest && !this._loader) {
    this._loader = ResourceHandler.createLoader(_0x12bba9, this._requestImage.bind(this, _0x12bba9), this._onError.bind(this));
  }
  this._url = _0x12bba9;
  this._image = new Image();
  this._loadingState = "decrypting";
  Crypto.decryptImg(_0x12bba9, this, Crypto.guard());
};
ImageManager.pngMeta = function (_0x4eabd7, _0x580428) {
  const _0x1d2e4d = [73, 68, 65, 84];
  const _0x35e047 = [116, 69, 88, 116];
  let _0x2eff2a = -1;
  let _0x219b57 = _0x4eabd7.length - 4;
  for (let _0x4b54ed = 0; _0x4b54ed <= _0x219b57; _0x4b54ed++) {
    _0x2eff2a = _0x4b54ed;
    for (let _0x1663a8 = 0; _0x1663a8 < 4; _0x1663a8++) {
      if (_0x4eabd7[_0x4b54ed + _0x1663a8] !== _0x1d2e4d[_0x1663a8]) {
        _0x2eff2a = -1;
        break;
      }
    }
    if (_0x2eff2a > -1) {
      break;
    }
  }
  if (_0x2eff2a < 0) {
    App.fail("IDAT chunk not found.");
    return "";
  }
  for (let _0x409dfe = 0; _0x409dfe < _0x2eff2a; _0x409dfe++) {
    if (_0x4eabd7[_0x409dfe] !== _0x35e047[0] || _0x4eabd7[_0x409dfe + 1] !== _0x35e047[1] || _0x4eabd7[_0x409dfe + 2] !== _0x35e047[2] || _0x4eabd7[_0x409dfe + 3] !== _0x35e047[3]) {
      continue;
    }
    let _0x5a22ef = Buffer.from(_0x4eabd7.slice(_0x409dfe - 4, _0x409dfe));
    let _0x511a3c = _0x5a22ef.readUInt32BE(0);
    let _0xdc5f44 = _0x4eabd7.slice(_0x409dfe + 4, _0x409dfe + 4 + _0x511a3c);
    let _0xa008f4 = _0xdc5f44.indexOf(0);
    let _0x48209e = String.fromCharCode.apply(null, _0xdc5f44.slice(0, _0xa008f4));
    let _0x4cfa54 = String.fromCharCode.apply(null, _0xdc5f44.slice(_0xa008f4 + 1));
    if (_0x48209e === _0x580428) {
      return _0x4cfa54;
    }
  }
  return "";
};
const _IM_LB = ImageManager.loadBitmap;
ImageManager.loadBitmap = function (_0x15a8cb, _0x1d4bc8, _0x14222c, _0xcb8d61) {
  _0x15a8cb = Lang.imgFolder(_0x15a8cb, _0x1d4bc8);
  return _IM_LB.call(this, _0x15a8cb, _0x1d4bc8, _0x14222c, _0xcb8d61);
};
const DEFAULT_INV = "ashley";
function Inventory() {}
Inventory.storage = {};
Inventory.current = DEFAULT_INV;
Inventory.swap = function (_0x42649a) {
  if (this.current == _0x42649a) {
    return;
  }
  var _0x3dd29c = {};
  var _0x1fffb5 = {};
  var _0x38bf22 = this.current;
  this.current = _0x42649a;
  $gameParty.allItems().forEach(function (_0x395ebe) {
    var _0x316f2f = $gameParty.numItems(_0x395ebe);
    _0x3dd29c[_0x395ebe.id] = _0x316f2f;
    $gameParty.loseItem(_0x395ebe, _0x316f2f);
  });
  this.storage[_0x38bf22] = _0x3dd29c;
  _0x1fffb5 = this.storage[this.current] || {};
  for (var _0x5e97ae in _0x1fffb5) {
    if (DataManager.isItem($dataItems[_0x5e97ae])) {
      $gameParty.gainItem($dataItems[_0x5e97ae], _0x1fffb5[_0x5e97ae], false);
    } else if (DataManager.isWeapon($dataWeapons[_0x5e97ae])) {
      $gameParty.gainItem($dataWeapons[_0x5e97ae], _0x1fffb5[_0x5e97ae], false);
    } else if (DataManager.isArmor($dataArmors[_0x5e97ae])) {
      $gameParty.gainItem($dataArmors[_0x5e97ae], _0x1fffb5[_0x5e97ae], false);
    }
  }
};
const _DM_CGO = DataManager.createGameObjects;
DataManager.createGameObjects = function () {
  _DM_CGO.call(this);
  Inventory.storage = {};
  Inventory.current = DEFAULT_INV;
};
const _DM_MSC = DataManager.makeSaveContents;
DataManager.makeSaveContents = function () {
  var _0x2cdaa0 = {};
  var _0x15414c = _DM_MSC.call(this);
  $gameParty.allItems().forEach(function (_0x4ccda4) {
    _0x2cdaa0[_0x4ccda4.id] = $gameParty.numItems(_0x4ccda4);
  });
  Inventory.storage[Inventory.current] = _0x2cdaa0;
  _0x15414c.format = FORMAT;
  _0x15414c.invStorage = Inventory.storage;
  _0x15414c.invCurrent = Inventory.current;
  return _0x15414c;
};
const _DM_ESC = DataManager.extractSaveContents;
DataManager.extractSaveContents = function (_0x1bc4de) {
  _DM_ESC.call(this, _0x1bc4de);
  Inventory.current = "";
  Inventory.storage = _0x1bc4de.invStorage || {};
  Inventory.swap(_0x1bc4de.invCurrent || "");
};
const MENU_ICON_MARGIN = 40;
function MenuOptions() {}
MenuOptions.iconImages = {};
MenuOptions.orderAndIcons = {
  "New Game": "new_game",
  Continue: "continue",
  Options: "options",
  Language: "language",
  "Vision Room": "vision",
  Credits: "credits",
  "Quit Game": "quit"
};
MenuOptions.labels = function () {
  return Object.keys(this.orderAndIcons);
};
MenuOptions.init = function () {
  this.labels().forEach(_0x1a81c3 => {
    this.iconImages[_0x1a81c3] = ImageManager.loadSystem(this.orderAndIcons[_0x1a81c3]);
  });
};
const _WTC_MCL = Window_TitleCommand.prototype.makeCommandList;
Window_TitleCommand.prototype.makeCommandList = function () {
  _WTC_MCL.call(this);
  if (Lang.count() > 1) {
    this.addCommand("Language", "language");
  }
  if (globalTag("vision_room")) {
    this.addCommand("Vision Room", "vision");
  }
  var _0xea564f = [];
  for (var _0x3eab02 of MenuOptions.labels()) {
    var _0x35c2c9 = this._list.findIndex(_0x3422f3 => _0x3422f3.name === _0x3eab02);
    if (_0x35c2c9 > -1) {
      _0xea564f.push(this._list[_0x35c2c9]);
    }
  }
  this._list = _0xea564f;
};
const _ST_CCW = Scene_Title.prototype.createCommandWindow;
Scene_Title.prototype.createCommandWindow = function () {
  _ST_CCW.call(this);
  this._commandWindow.setHandler("language", this.commandLanguage.bind(this));
  this._commandWindow.setHandler("vision", this.commandVisionRoom.bind(this));
};
Scene_Title.prototype.commandLanguage = function () {
  this._commandWindow.close();
  SceneManager.push(Scene_Language);
};
Scene_Title.prototype.commandVisionRoom = function () {
  DataManager.setupNewGame();
  $gamePlayer.reserveTransfer(86, 13, 9);
  this._commandWindow.close();
  this.fadeOutAll();
  SceneManager.goto(Scene_Map);
};
Window_TitleCommand.prototype.drawItem = function (_0x4f7488) {
  var _0x149255 = this.commandName(_0x4f7488);
  var _0x4a5e57 = this.itemRectForText(_0x4f7488);
  var _0x1ce98c = MenuOptions.iconImages[_0x149255];
  if (_0x1ce98c) {
    var _0x4c28e2 = _0x4a5e57.x;
    var _0x41ca12 = _0x4a5e57.y + (_0x4a5e57.height - _0x1ce98c.height) / 2;
    this.contents.blt(_0x1ce98c, 0, 0, _0x1ce98c.width, _0x1ce98c.height, _0x4c28e2, _0x41ca12);
  }
  _0x4a5e57.x += MENU_ICON_MARGIN;
  this.resetTextColor();
  this.changePaintOpacity(this.isCommandEnabled(_0x4f7488));
  this.drawText(Lang.translate(_0x149255), _0x4a5e57.x, _0x4a5e57.y, _0x4a5e57.width, "left");
};
var _sceneMenu = Scene_NCMenu || Scene_Menu;
var _windowMenu = Window_NCMenu || Window_MenuCommand;
const _WM_MCL = _windowMenu.prototype.makeCommandList;
_windowMenu.prototype.makeCommandList = function () {
  _WM_MCL.call(this);
  var _0x48a624 = this._list.findIndex(_0x1b30f2 => _0x1b30f2.symbol === "options");
  var _0x3cca79 = this._list.slice();
  this.clearCommandList();
  _0x3cca79.forEach((_0x35abe5, _0x352dc7) => {
    this.addCommand(_0x35abe5.name, _0x35abe5.symbol, _0x35abe5.enabled, _0x35abe5.ext);
    if (_0x352dc7 === _0x48a624) {
      if (Lang.count() > 1) {
        this.addCommand("Language", "language", true, null);
      }
      this.addCommand("Controls", "controls", true, null);
    }
  });
};
_windowMenu.prototype.drawItem = function (_0x2ebabb) {
  var _0x64efeb = this.itemRectForText(_0x2ebabb);
  var _0x42234c = this.windowWidth();
  var _0x71f600 = _0x64efeb.width - _0x42234c;
  var _0x37c876 = this.commandName(_0x2ebabb);
  _0x37c876 = Lang.translate(_0x37c876);
  this.resetTextColor();
  this.changePaintOpacity(this.isCommandEnabled(_0x2ebabb));
  this.drawText(_0x37c876, _0x64efeb.x, _0x64efeb.y, _0x71f600, "left");
};
const _SM_CCW = _sceneMenu.prototype.createCommandWindow;
_sceneMenu.prototype.createCommandWindow = function () {
  _SM_CCW.call(this);
  this._commandWindow.setHandler("language", this.commandLanguage.bind(this));
  this._commandWindow.setHandler("controls", this.commandControls.bind(this));
};
_sceneMenu.prototype.commandLanguage = function () {
  this._commandWindow.close();
  SceneManager.push(Scene_Language);
};
_sceneMenu.prototype.commandControls = function () {
  SceneManager.push(Scene_Controls);
};
function Window_Language() {
  this.initialize.apply(this, arguments);
}
Window_Language.prototype = Object.create(Window_Command.prototype);
Window_Language.prototype.constructor = Window_Language;
Window_Language.prototype.initialize = function () {
  Window_Command.prototype.initialize.call(this, 0, 0);
  this.x = (Graphics.boxWidth - this.width) / 2;
  this.y = (Graphics.boxHeight - this.height) / 2;
  this.currentLanguage = ConfigManager.language;
};
Window_Language.prototype.makeCommandList = function () {
  this.addCommand("Language", "language", true, 0);
};
Window_Language.prototype.windowWidth = function () {
  return 400;
};
Window_Language.prototype.numVisibleRows = function () {
  return 5;
};
Window_Language.prototype.drawItem = function (_0x21ab7b) {
  this.refresh();
};
const BKG_COLOR = "#182232";
const TXT_COLOR = "#30B0CF";
const NFO_COLOR = "#EBAE69";
const SEP_COLOR = "#AAAAAA";
Window_Language.prototype.refresh = function () {
  var _0x1002e7 = LANG_ICO_MARGIN;
  var _0x13eee8 = LANG_ICO_PIXELS;
  var _0x4b8357 = this.itemRect(0);
  var _0x4a5acd = this.lineHeight();
  var _0x21d0b2 = ConfigManager.language;
  this.contents.clear();
  this.resetTextColor();
  this.resetFontSettings();
  this.drawText(Lang.translate(this.commandName(0)), _0x4b8357.x + _0x13eee8 + _0x1002e7, _0x4b8357.y, _0x4b8357.width - 8 - _0x13eee8 - _0x1002e7, "left");
  this.swapFont(_0x21d0b2);
  this.drawText(Lang.property(_0x21d0b2, "langName"), _0x4b8357.x + _0x13eee8 + _0x1002e7, _0x4b8357.y, _0x4b8357.width - 8 - _0x13eee8 - _0x1002e7, "right");
  if (Lang.isOfficial(_0x21d0b2)) {
    this.contents.fillRect(_0x4b8357.x, _0x4b8357.y + _0x4a5acd, _0x4b8357.width, _0x4a5acd, BKG_COLOR);
    this.resetFontSettings();
    this.contents.fontBold = true;
    this.changeTextColor(TXT_COLOR);
    this.drawText(Lang.translate("Credits"), _0x4b8357.x + _0x13eee8 + _0x1002e7, _0x4b8357.y + _0x4a5acd, _0x4b8357.width - 8 - _0x13eee8, "left");
    this.contents.fontBold = false;
    var _0x2f6d15 = this.contents;
    var _0x586d62 = ImageManager.loadSystem("stamp");
    _0x586d62.addLoadListener(function () {
      _0x2f6d15.blt(_0x586d62, 0, 0, _0x13eee8, _0x13eee8, _0x4b8357.x, _0x4b8357.y + _0x4a5acd + (_0x4a5acd - _0x13eee8) / 2);
    });
    this.changeTextColor(NFO_COLOR);
  } else {
    this.resetTextColor();
    this.contents.fillRect(_0x4b8357.x, _0x4b8357.y + _0x4a5acd + (_0x4a5acd - 6) / 2, _0x4b8357.width, 3, SEP_COLOR);
  }
  this.swapFont(_0x21d0b2);
  var _0x38dc36 = Lang.property(_0x21d0b2, "langInfo");
  for (var _0x47134f = 0; _0x47134f < Math.min(_0x38dc36.length, 3); _0x47134f++) {
    var _0x63d8d2 = _0x38dc36[_0x47134f];
    this.drawText(_0x63d8d2, _0x4b8357.x + _0x13eee8 + _0x1002e7, _0x4b8357.y + _0x4a5acd * (_0x47134f + 2), _0x4b8357.width - 8 - _0x13eee8, "left");
  }
  this.resetFontSettings();
};
Window_Language.prototype.swapFont = function (_0x2c5f7c) {
  var _0x2242cd = Font.resolve(Lang.property(_0x2c5f7c, "fontFace"));
  if (_0x2242cd !== "") {
    this.contents.fontFace = _0x2242cd;
    this.contents.fontSize = Lang.property(_0x2c5f7c, "fontSize");
  } else {
    this.contents.fontSize = this.standardFontSize();
    this.contents.fontFace = this.standardFontFace();
  }
};
Window_Language.prototype.processOk = function () {
  this._input(INPUT_RIGHT);
};
Window_Language.prototype.cursorRight = function (_0x1b6aab) {
  this._input(INPUT_RIGHT);
};
Window_Language.prototype.cursorLeft = function (_0x16564d) {
  this._input(INPUT_LEFT);
};
Window_Language.prototype._input = function (_0x2e92c5) {
  var _0x5acd85 = Lang.count();
  var _0x2cd43d = Lang.index(ConfigManager.language);
  this.changeValue("language", Lang.key((_0x2cd43d + _0x2e92c5 + _0x5acd85) % _0x5acd85));
};
Window_Language.prototype.changeValue = function (_0x15917a, _0x163926) {
  SoundManager.playSave();
  Lang.select(_0x163926);
  ConfigManager.symbol = _0x163926;
  this.redrawItem(this.findSymbol(_0x15917a));
};
Window_Language.prototype.update = function () {
  Window_Command.prototype.update.call(this);
  if (Input.isTriggered("cancel")) {
    if (this.currentLanguage != ConfigManager.language) {
      ConfigManager.save();
      Lang.select(ConfigManager.language);
    }
    SceneManager.pop();
    SoundManager.playCancel();
  }
};
function Scene_Language() {
  this.initialize.apply(this, arguments);
}
Scene_Language.prototype = Object.create(Scene_MenuBase.prototype);
Scene_Language.prototype.constructor = Scene_Language;
Scene_Language.prototype.create = function () {
  Scene_MenuBase.prototype.create.call(this);
  this.setBackgroundOpacity(128);
  this._languageWindow = new Window_Language();
  this.addWindow(this._languageWindow);
};
Window_Options.prototype.addGeneralOptions = function () {
  this.addCommand("UI Hints", "inputHint");
  this.addCommand("Text Speed", "textSpeed");
  this.addCommand("Auto Saves", "autoSaves");
  this.addCommand("Fullscreen", "fullscreen");
  this.addCommand("Run Always", "alwaysDash");
  this.addCommand("Run Speed", "dashBonus");
};
Window_Options.prototype.addVolumeOptions = function () {
  this.addCommand("Volume BGM", "bgmVolume");
  this.addCommand("Volume SFX", "seVolume");
};
Window_Options.prototype.statusText = function (_0x2bf739) {
  let _0x4a3f2a = this.commandSymbol(_0x2bf739);
  let _0x3d23c4 = this.getConfigValue(_0x4a3f2a);
  const _0x56ab6b = Lang.translate("On");
  const _0x15bf1f = Lang.translate("Off");
  const _0xffb19f = Lang.translate("Slow");
  const _0x9bba11 = Lang.translate("Fast");
  const _0x17f2b0 = Lang.translate("Instant");
  if (_0x4a3f2a === "dashBonus") {
    return _0x3d23c4 + "%";
  }
  if (_0x4a3f2a === "inputHint" || _0x4a3f2a === "fullscreen" || _0x4a3f2a === "alwaysDash") {
    if (_0x3d23c4) {
      return _0x56ab6b;
    } else {
      return _0x15bf1f;
    }
  }
  if (_0x4a3f2a === "bgmVolume" || _0x4a3f2a === "bgsVolume" || _0x4a3f2a === "meVolume" || _0x4a3f2a === "seVolume") {
    return _0x3d23c4 + "%";
  }
  if (_0x4a3f2a === "walkSpeed") {
    return _0x3d23c4.toFixed(2);
  }
  if (_0x4a3f2a === "textSpeed") {
    return [_0xffb19f, _0x9bba11, _0x17f2b0][_0x3d23c4];
  }
  return _0x3d23c4;
};
const INPUT_LEFT = -1;
const INPUT_RIGHT = 1;
const WRAP_RESULT = true;
Window_Options.prototype.processOk = function () {
  this._input(INPUT_RIGHT, WRAP_RESULT);
};
Window_Options.prototype.cursorRight = function (_0x266b16) {
  this._input(INPUT_RIGHT);
};
Window_Options.prototype.cursorLeft = function (_0x15ed58) {
  this._input(INPUT_LEFT);
};
Window_Options.prototype._input = function (_0xf4215, _0x5dca9a = false) {
  let _0x437b5e = this.index();
  let _0x2a4ae7 = this.commandSymbol(_0x437b5e);
  let _0x2c5cbe = this.getConfigValue(_0x2a4ae7);
  if (_0x2a4ae7 === "inputHint" || _0x2a4ae7 === "fullscreen" || _0x2a4ae7 === "alwaysDash") {
    this.changeValue(_0x2a4ae7, !_0x2c5cbe);
    return;
  }
  let _0x225cbc = 0;
  let _0x532c56 = 100;
  if (_0x2a4ae7.contains("Volume")) {
    _0xf4215 *= VOL_STEP_SIZE;
  } else if (_0x2a4ae7 === "textSpeed") {
    _0x532c56 = 2;
  } else if (_0x2a4ae7 === "autoSaves") {
    _0x532c56 = DataManager.autoSaveMax();
  } else if (_0x2a4ae7 === "dashBonus") {
    _0x225cbc = 10;
    _0x532c56 = 30;
    _0xf4215 *= DSH_STEP_SIZE;
  }
  if (_0x5dca9a) {
    _0x2c5cbe = _0x2c5cbe.boundaryWrap(_0xf4215, _0x225cbc, _0x532c56);
  } else {
    _0x2c5cbe = (_0x2c5cbe + _0xf4215).clamp(_0x225cbc, _0x532c56);
  }
  this.changeValue(_0x2a4ae7, _0x2c5cbe);
};
Window_Options.prototype.changeValue = function (_0x4a8f85, _0x416952) {
  if (_0x4a8f85 === "fullscreen") {
    this.setConfigValue(_0x4a8f85, _0x416952);
    if (_0x416952) {
      Graphics._requestFullScreen();
    } else {
      Graphics._cancelFullScreen();
    }
  } else {
    this.setConfigValue(_0x4a8f85, _0x416952);
    if (_0x4a8f85 === "bgmVolume") {
      ConfigManager.meVolume = _0x416952;
    } else if (_0x4a8f85 === "seVolume") {
      ConfigManager.bgsVolume = _0x416952;
    }
  }
  SoundManager.playEquip();
  this.redrawItem(this.findSymbol(_0x4a8f85));
};
Window_Options.prototype.drawItem = function (_0x4897e2) {
  let _0x458158 = this.itemRectForText(_0x4897e2);
  let _0x1bddcd = this.statusWidth();
  let _0x4476cf = _0x458158.width - _0x1bddcd;
  let _0xc1209f = this.commandName(_0x4897e2);
  _0xc1209f = Lang.translate(_0xc1209f);
  this.resetTextColor();
  this.changePaintOpacity(this.isCommandEnabled(_0x4897e2));
  this.drawText(_0xc1209f, _0x458158.x, _0x458158.y, _0x4476cf, "left");
  this.drawText(this.statusText(_0x4897e2), _0x4476cf, _0x458158.y, _0x1bddcd, "right");
};
function Scene_Controls() {
  this.initialize.apply(this, arguments);
}
Scene_Controls.prototype = Object.create(Scene_Base.prototype);
Scene_Controls.prototype.constructor = Scene_Controls;
Scene_Controls.prototype.initialize = function () {
  Scene_Base.prototype.initialize.call(this);
};
Scene_Controls.prototype._img = null;
Scene_Controls.prototype.create = function () {
  Scene_Base.prototype.create.call(this);
  if (!this._img) {
    this._img = ImageManager.loadPicture("keys");
  }
  this._background = new Sprite(this._img);
  this.addChild(this._background);
};
Scene_Controls.prototype.update = function () {
  Scene_Base.prototype.update.call(this);
  if (Input.isTriggered("ok") || Input.isTriggered("cancel") || TouchInput.isTriggered()) {
    this.popScene();
  }
};
Scene_Controls.prototype.terminate = function () {
  SoundManager.playCancel();
  Scene_Base.prototype.terminate.call(this);
  this.removeChild(this._background);
  this._background = null;
};
const _G_SFS = Graphics._switchFullScreen;
Graphics._switchFullScreen = function () {
  _G_SFS.call(this);
  ConfigManager.fullscreen = Graphics._isFullScreen();
  ConfigManager.save();
  if (Graphics._isFullScreen()) {
    document.body.style.cursor = "none";
  } else {
    document.body.style.cursor = "default";
  }
  if (SceneManager._scene instanceof Scene_Options) {
    var _0x5b9af6 = SceneManager._scene._optionsWindow;
    _0x5b9af6.redrawItem(_0x5b9af6.findSymbol("fullscreen"));
  }
};
Game_CharacterBase.prototype.realMoveSpeed = function () {
  if (this._moveSpeed != 4) {
    return this._moveSpeed;
  }
  var _0x1917e7 = BASE_WALK_SPD;
  if (this.isDashing()) {
    var _0x34ba4d = ConfigManager.dashBonus;
    _0x1917e7 *= 1 + _0x34ba4d / 100;
  }
  return _0x1917e7;
};
const _WM_USF = Window_Message.prototype.updateShowFast;
Window_Message.prototype.updateShowFast = function () {
  var _0xa122ef = Math.abs(ConfigManager.textSpeed - 2);
  if (_0xa122ef === 0) {
    this._showFast = true;
  } else {
    _WM_USF.call(this);
  }
};
const _WM_UM = Window_Message.prototype.updateMessage;
Window_Message.prototype.updateMessage = function () {
  var _0xe8c53a = Math.abs(ConfigManager.textSpeed - 2);
  var _0x370b46 = _WM_UM.call(this);
  if (_0xe8c53a !== 0 && this._textState && this._textState.text[this._textState.index] !== "") {
    this._waitCount = _0xe8c53a - 1;
  }
  return _0x370b46;
};
const _I_UGS = Input._updateGamepadState;
Input._updateGamepadState = function (_0x27e9b9) {
  const _0xbe1ed5 = 9;
  if (_0x27e9b9.buttons[_0xbe1ed5] && _0x27e9b9.buttons[_0xbe1ed5].pressed !== null) {
    var _0x47d626 = this._gamepadStates[_0xbe1ed5];
    var _0x5e4904 = _0x27e9b9.buttons[_0xbe1ed5].pressed;
    this._gamepadStates[_0xbe1ed5] = _0x5e4904;
    if (!_0x47d626 && _0x5e4904) {
      Graphics._switchFullScreen();
    }
  }
  _I_UGS.call(this, _0x27e9b9);
};
Sprite_Balloon.prototype.setup = function (_0xec78f3) {
  this._balloonId = _0xec78f3;
  this._duration = this.speed() * 8 + this.waitTime();
  this._loop = false;
  this._fullDuration = this._duration;
};
Sprite_Balloon.prototype.frameIndex = function () {
  var _0x3547d = this._duration / this._fullDuration;
  var _0x42a367 = Math.floor((1 - _0x3547d) * 8);
  return Math.max(0, Math.min(7, _0x42a367));
};
Sprite_Balloon.prototype.resetAnimation = function () {
  this._duration = this._fullDuration;
};
Sprite_Balloon.prototype.update = function () {
  Sprite_Base.prototype.update.call(this);
  if (this._duration > 0) {
    this._duration--;
    if (this._loop) {
      this.updateFrame();
      if (this._duration <= 0) {
        this._duration += this._fullDuration;
      }
    } else if (this._duration > 0) {
      this.updateFrame();
    }
  }
};
Game_Event.prototype.isEnabled = function () {
  var _0x5eed63 = this.event().pages[this._pageIndex];
  if (_0x5eed63.list.length < 1 || _0x5eed63.list.length === 1 && _0x5eed63.list[0].code === 0) {
    return false;
  }
  var _0x5c4562 = _0x5eed63.list[0];
  if (_0x5c4562.code === 108) {
    var _0x31b638 = _0x5c4562.parameters[0].toLowerCase().replace(/\s+/g, "");
    var _0x148232 = _0x31b638.match(/enabled:(\d+)([!=><]+)(\w+|has)/);
    if (!_0x148232) {
      return false;
    }
    var _0x3be401 = Number(_0x148232[1]);
    var _0x58b032 = _0x148232[2];
    var _0x56c995 = _0x148232[3];
    if (_0x56c995 === "on" || _0x56c995 === "off") {
      var _0x42ae81 = _0x56c995 === "on";
      var _0x1ccabe = $gameSwitches.value(_0x3be401);
      if (_0x58b032 === "==") {
        return _0x42ae81 && _0x1ccabe || !_0x42ae81 && !_0x1ccabe;
      }
      if (_0x58b032 === "!=") {
        return _0x42ae81 && !_0x1ccabe || !_0x42ae81 && _0x1ccabe;
      }
      return false;
    } else if (_0x56c995 === "has") {
      if (_0x58b032 === "==") {
        return $gameParty.hasItem($dataItems[_0x3be401]);
      }
      if (_0x58b032 === "!=") {
        return !$gameParty.hasItem($dataItems[_0x3be401]);
      }
      return false;
    } else {
      var _0x2dbe1b = $gameVariables.value(_0x3be401);
      var _0x3b3428 = Number(_0x56c995);
      switch (_0x58b032) {
        case "==":
          return _0x2dbe1b === _0x3b3428;
        case "!=":
          return _0x2dbe1b !== _0x3b3428;
        case ">=":
          return _0x2dbe1b >= _0x3b3428;
        case "<=":
          return _0x2dbe1b <= _0x3b3428;
        case ">":
          return _0x2dbe1b > _0x3b3428;
        case "<":
          return _0x2dbe1b < _0x3b3428;
      }
      App.fail("Invalid comparison operator for enable / disable comment hint: " + _0x5c4562.parameters[0]);
      return false;
    }
  }
  return true;
};
const HINT_Y_OFS = 78;
const BALLOON_ID = 15;
const HINT_RANGE = 0.75;
function eventHintEnable(_0x22a404) {}
function eventHintDisable(_0x76f8af) {}
function Hint() {}
;
Hint._change = 0;
Hint._active = false;
Hint._balloon = null;
Hint.delta = function () {
  if (Hint._change > 0) {
    var _0x475c3b = SceneManager._deltaTime;
    return _0x475c3b / Hint._change;
  }
  return 0;
};
Hint.balloon = function () {
  if (!this._balloon) {
    this._balloon = new Sprite_Balloon();
    this._balloon.setup(BALLOON_ID);
    this._balloon._loop = true;
    this._balloon.alpha = 0;
    this._change = 0;
    this._active = false;
  }
  return this._balloon;
};
Hint.open = function (_0x3a1b27 = 0) {
  this._active = true;
  this._change = Math.max(0, _0x3a1b27);
  let _0x134e06 = this.balloon();
  if (_0x134e06.alpha <= 0) {
    _0x134e06._duration = _0x134e06._fullDuration;
  }
  if (_0x134e06.alpha < 1 && _0x3a1b27 <= 0) {
    _0x134e06.alpha = 1;
  }
};
Hint.close = function (_0xd9d1b5 = 0) {
  this._active = false;
  this._change = Math.max(0, _0xd9d1b5);
  let _0x3003b7 = this.balloon();
  if (_0x3003b7.alpha > 0 && _0xd9d1b5 <= 0) {
    _0x3003b7.alpha = 0;
  }
};
Hint.process = function () {
  let _0xae984e = this.balloon();
  if (!ConfigManager.inputHint) {
    Hint.close();
    return;
  }
  let _0x520172 = $gameMap._interpreter._eventId;
  if (_0x520172 > 0) {
    let _0x54859c = $gameMap.event(_0x520172) || null;
    if (!_0x54859c || _0x54859c._trigger < 4) {
      Hint.close();
      return;
    }
  }
  if (this._active) {
    _0xae984e.update();
    _0xae984e.alpha = Math.min(1, _0xae984e.alpha + Hint.delta());
  } else {
    _0xae984e.alpha = Math.max(0, _0xae984e.alpha - Hint.delta());
  }
  _0xae984e.x = $gamePlayer.screenX();
  _0xae984e.y = $gamePlayer.screenY() - HINT_Y_OFS;
  let _0x4999a9 = $gamePlayer.x;
  let _0x2fc359 = $gamePlayer.y;
  let _0x1f4343 = $gamePlayer._realX;
  let _0x4a768e = $gamePlayer._realY;
  let _0x30f2e5 = _0x4999a9;
  let _0x2139e3 = _0x2fc359;
  switch ($gamePlayer.direction()) {
    case 2:
      _0x2139e3++;
      break;
    case 4:
      _0x30f2e5--;
      break;
    case 6:
      _0x30f2e5++;
      break;
    case 8:
      _0x2139e3--;
      break;
  }
  let _0x488c76 = 0;
  $gameMap.eventsXy(_0x4999a9, _0x2fc359).forEach(function (_0x5d2b9d) {
    if (_0x5d2b9d.isTriggerIn([0]) && _0x5d2b9d.isEnabled()) {
      _0x488c76 = _0x5d2b9d._eventId;
      return;
    }
  });
  $gameMap.eventsXy(_0x30f2e5, _0x2139e3).forEach(function (_0x19d7df) {
    if (_0x488c76 > 0) {
      return;
    }
    if (_0x19d7df.isTriggerIn([0]) && _0x19d7df.isNormalPriority() && _0x19d7df.isEnabled()) {
      _0x488c76 = _0x19d7df._eventId;
      return;
    }
  });
  if (_0x488c76 > 0) {
    let _0x42f0ca = 1 - Math.abs(_0x4999a9 - _0x1f4343);
    let _0x28ae26 = 1 - Math.abs(_0x2fc359 - _0x4a768e);
    if (Math.min(_0x42f0ca, _0x28ae26) > HINT_RANGE) {
      if (!this._active) {
        this.open(0.1);
        this.lastEvent = _0x488c76;
      }
    } else if (_0x488c76 != this.lastEvent) {
      this.close(0.2);
    }
  } else {
    this.lastEvent = 0;
    if (this._active) {
      this.close(0.1);
    }
  }
};
const _SM_S = Scene_Map.prototype.start;
Scene_Map.prototype.start = function () {
  _SM_S.call(this);
  this.addChild(Hint.balloon());
};
var _SC_US = SceneManager.updateScene;
SceneManager.updateScene = function () {
  _SC_US.call(this);
  if (!this.isSceneChanging() && this.isCurrentSceneStarted() && this._scene instanceof Scene_Map) {
    Hint.process();
    return;
  }
  Hint.close();
};
const FONT_TYPES = [".ttf", ".otf", ".eot", ".svg", ".woff", ".woff2"];
function Font() {}
Font.data = {};
Font.size = 28;
Font.face = "GameFont";
Font.list = ["Dotum", "SimHei", "GameFont", "Heiti TC", "sans-serif", "AppleGothic"];
Font.change = function (_0x55384e, _0x11e4ff = 28) {
  this.face = "";
  var _0x5e462c = this.resolve(_0x55384e);
  if (_0x5e462c) {
    this.face = _0x5e462c;
    this.size = _0x11e4ff;
  }
};
Font.key = function (_0x1128e9) {
  var _0x243a61 = new RegExp(_0x1128e9, "i");
  var _0x3efe92 = Object.keys(this.data).filter(_0xd07f1a => _0x243a61.test(_0xd07f1a));
  if (_0x3efe92.length > 0) {
    return _0x3efe92[0];
  }
  return _0x1128e9;
};
Font.resolve = function (_0x4bea33) {
  var _0x406612 = Utils.filename(_0x4bea33);
  if (this.list.includes(_0x406612)) {
    return _0x406612;
  }
  var _0x34d6ab = this.key(Lang.current() + "_" + _0x406612);
  if (!_0x34d6ab) {
    _0x34d6ab = this.key(_0x406612);
  }
  if (!_0x34d6ab) {
    App.fail("Cannot locate font named: " + _0x4bea33);
    return "GameFont";
  }
  var _0x43e7d3 = this.data[_0x34d6ab];
  if (!Utils.exists(_0x43e7d3)) {
    App.fail("Missing font: " + _0x43e7d3);
    return "GameFont";
  }
  _0x43e7d3 = Utils.relative(App.rootPath(), _0x43e7d3);
  _0x43e7d3 = _0x43e7d3.replace(/\\/g, "/");
  _0x34d6ab = _0x34d6ab.replace(/\.[^/.]+$/, "");
  if (this.list.includes(_0x34d6ab)) {
    return _0x34d6ab;
  }
  var _0x1b0cbe = new FontFace(_0x34d6ab, "url(" + _0x43e7d3 + ")");
  _0x1b0cbe.load().then(function (_0x4ae606) {
    document.fonts.add(_0x4ae606);
    Font.list.push(_0x34d6ab);
    if (SceneManager._scene._windowLayer) {
      SceneManager._scene._windowLayer.children.forEach(function (_0x2ae33d) {
        if (typeof _0x2ae33d.refresh === "function") {
          _0x2ae33d.refresh();
        }
      });
    }
  }).catch(function (_0x444843) {
    App.fail("Font failed to load: " + _0x4bea33, _0x444843);
    return "GameFont";
  });
  return _0x34d6ab;
};
const _WB_RFS = Window_Base.prototype.resetFontSettings;
Window_Base.prototype.resetFontSettings = function () {
  _WB_RFS.apply(this, arguments);
  if (Font.face !== "" && this.constructor.name !== "Window_Credits") {
    this.contents.fontFace = Font.face;
    this.contents.fontSize = Font.size;
  }
};
const LANG_DIR = "languages/";
const LANG_LOC = "english";
const LANG_TXT = "english_txt";
const LANG_CSV = "english_csv";
const LANG_ICO_MARGIN = 10;
const LANG_ICO_PIXELS = 26;
const VALID_EXT = [".loc", ".txt", ".csv"];
const LANG_ORDERING = ["english", "korean", "japanese", "chinese"];
function Lang() {}
Lang.data = {};
Lang.list = {};
Lang.offc = [];
Lang.count = function () {
  return Object.keys(this.list).length;
};
Lang.index = function (_0x69c98e) {
  return Object.keys(this.list).indexOf(_0x69c98e);
};
Lang.isOfficial = function (_0x394e2) {
  return this.offc.includes(_0x394e2);
};
Lang.current = function () {
  var _0x224b68 = this.list[ConfigManager.language] || "";
  if (!_0x224b68) {
    App.warn("Current language not in list: " + key);
    return "n/a";
  }
  return Utils.basename(Utils.dirname(_0x224b68));
};
Lang.key = function (_0x4f1c91) {
  var _0x4f47fa = Object.keys(this.list);
  var _0x89612d = _0x4f47fa.length - 1;
  if (_0x89612d < 0) {
    App.crash("No language table created.");
    return "";
  }
  if (_0x4f1c91 < 0 || _0x4f1c91 > _0x89612d) {
    App.fail("Language index out of bounds.");
    _0x4f1c91 = 0;
  }
  return _0x4f47fa[_0x4f1c91];
};
Lang.property = function (_0x2b35b9, _0x36137d, _0x3a0d02 = null) {
  if (Object.keys(this.data).length === 0) {
    return _0x3a0d02;
  }
  if (this.data.hasOwnProperty(_0x2b35b9) && typeof this.data[_0x2b35b9] === "object" && this.data[_0x2b35b9].hasOwnProperty(_0x36137d)) {
    return this.data[_0x2b35b9][_0x36137d];
  }
  App.fail("Language property missing: " + _0x2b35b9 + ":" + _0x36137d);
  return _0x3a0d02;
};
Lang.translate = function (_0x613676) {
  var _0x416bf4 = ConfigManager.language;
  var _0x549b36 = this.property(_0x416bf4, "sysMenus");
  var _0x3e2e68 = this.property(_0x416bf4, "sysLabel");
  if (_0x549b36 && _0x549b36.hasOwnProperty(_0x613676)) {
    return _0x549b36[_0x613676];
  }
  if (_0x3e2e68 && _0x3e2e68.hasOwnProperty(_0x613676)) {
    return _0x3e2e68[_0x613676];
  }
  return _0x613676;
};
Lang.label = function (_0x4c3a30, _0x170b12 = false) {
  var _0x25a031 = this.property(ConfigManager.language, "labelLUT");
  _0x4c3a30 = _0x4c3a30.replace(/\(label\)\[([^\]]+)\]/g, function (_0x3c9b92, _0x33b303) {
    var _0x28fdaf = _0x25a031 && _0x25a031[_0x33b303] ? _0x25a031[_0x33b303] : _0x3c9b92;
    if (_0x170b12) {
      return "<" + _0x28fdaf + ">";
    } else {
      return _0x28fdaf;
    }
  });
  return _0x4c3a30;
};
Lang.lines = function (_0xc1b2f1) {
  var _0x552c97 = {
    text: _0xc1b2f1,
    lines: []
  };
  var _0x8b527e = this.property(ConfigManager.language, "linesLUT");
  _0x552c97.text = _0x552c97.text.replace(/\(lines\)\[([^\]]+)\]/g, function (_0x34c756, _0x5e2aa6) {
    if (_0x8b527e && _0x8b527e[_0x5e2aa6]) {
      _0x552c97.lines = _0x8b527e[_0x5e2aa6];
      return "";
    }
    return _0x34c756;
  });
  return _0x552c97;
};
Lang.search = function () {
  let _0x1bd378 = Utils.join(App.rootPath(), LANG_DIR);
  if (!Utils.exists(_0x1bd378)) {
    App.warn("Language data unavailable.");
    return;
  }
  if (!Utils.canAccess(_0x1bd378)) {
    App.crash("Language data not accessible.");
    return;
  }
  let _0x775f7b = Utils.folders(_0x1bd378);
  if (_0x775f7b.length === 0) {
    App.crash("Error reading languages folder.");
    return;
  }
  this.list = {};
  for (let _0x59bf2b = 0; _0x59bf2b < _0x775f7b.length; _0x59bf2b++) {
    let _0xa7cc68 = Utils.join(_0x1bd378, _0x775f7b[_0x59bf2b]);
    for (let _0x3d4a2b of Utils.findFiles(_0xa7cc68, FONT_TYPES)) {
      let _0x5b8b12 = Utils.basename(_0x3d4a2b);
      let _0x1f8b08 = _0x775f7b[_0x59bf2b] + "_" + _0x5b8b12;
      Font.data[_0x1f8b08] = _0x3d4a2b;
    }
    let _0x48c0bf = Utils.files(_0xa7cc68);
    for (let _0xc04b20 = 0; _0xc04b20 < _0x48c0bf.length; _0xc04b20++) {
      let _0x149e2c = _0x48c0bf[_0xc04b20];
      let _0x5a719a = _0x775f7b[_0x59bf2b];
      let _0x32919c = Utils.ext(_0x149e2c).toLowerCase();
      if (!VALID_EXT.includes(_0x32919c)) {
        continue;
      }
      if (_0x32919c === ".loc") {
        this.offc.push(_0x5a719a);
      } else {
        _0x5a719a += _0x32919c.replace(".", "_");
        if (Utils.isOptionValid("test")) {
          this.offc.push(_0x5a719a);
        }
      }
      this.list[_0x5a719a] = Utils.join(_0xa7cc68, _0x149e2c);
    }
  }
  let _0x408ad0 = [];
  let _0x252822 = [];
  let _0x25ced1 = {};
  for (let _0x2dee6c in this.list) {
    if (this.offc.includes(_0x2dee6c)) {
      _0x252822.push(_0x2dee6c);
    } else {
      _0x408ad0.push(_0x2dee6c);
    }
  }
  for (let _0x1421b8 of LANG_ORDERING) {
    for (let _0x48ab3c of _0x252822) {
      if (_0x48ab3c.toLowerCase().contains(_0x1421b8.toLowerCase())) {
        _0x25ced1[_0x48ab3c] = this.list[_0x48ab3c];
      }
    }
  }
  for (let _0x394c66 of _0x252822) {
    _0x25ced1[_0x394c66] = this.list[_0x394c66];
  }
  for (let _0x27ecd7 of _0x408ad0) {
    _0x25ced1[_0x27ecd7] = this.list[_0x27ecd7];
  }
  this.list = _0x25ced1;
};
Lang.select = function (_0x20ce2e) {
  if (this.count() < 1) {
    if (App.isDevMode()) {
      App.info("Skipped language select: " + _0x20ce2e);
    } else {
      App.crash("Language data missing.\nA re-install may fix it.");
    }
    return;
  }
  var _0x1aff4b = [_0x20ce2e, Steam.currentLanguage(), LANG_LOC, LANG_TXT, LANG_CSV];
  for (var _0x43f97f of _0x1aff4b) {
    if (!this.list.hasOwnProperty(_0x43f97f)) {
      continue;
    }
    var _0x423c1e = {};
    var _0x444f64 = this.list[_0x43f97f];
    var _0x572dd2 = Utils.ext(_0x444f64).toLowerCase();
    if (this.data.hasOwnProperty(_0x43f97f)) {
      _0x423c1e = this.data[_0x43f97f];
    } else {
      if (_0x572dd2 === ".loc") {
        _0x423c1e = this.loadLOC(_0x444f64);
      } else if (_0x572dd2 === ".txt") {
        _0x423c1e = this.loadTXT(_0x444f64);
      } else if (_0x572dd2 === ".csv") {
        _0x423c1e = this.loadCSV(_0x444f64);
      }
      if (!this.isValid(_0x423c1e)) {
        App.fail("Invalid data for: " + _0x43f97f);
        continue;
      }
      this.data[_0x43f97f] = _0x423c1e;
      this.imgMapping(_0x43f97f);
    }
    ConfigManager.language = _0x43f97f;
    Font.change(_0x423c1e.fontFace, _0x423c1e.fontSize);
    var _0x24c0a6 = _0x423c1e.sysLabel.Game;
    var _0x4aa497 = _0x423c1e.sysLabel.Item;
    var _0x5547a4 = _0x423c1e.sysLabel.File;
    var _0x37c73a = _0x423c1e.sysLabel.Save;
    var _0xb46f80 = _0x423c1e.sysLabel.Load;
    document.title = _0x24c0a6;
    $dataSystem.gameTitle = _0x24c0a6 + " v" + VERSION;
    $dataSystem.terms.commands[4] = _0x4aa497;
    $dataSystem.terms.messages.file = _0x5547a4;
    $dataSystem.terms.messages.saveMessage = _0x37c73a;
    $dataSystem.terms.messages.loadMessage = _0xb46f80;
    return;
  }
  ConfigManager.language = "";
  const _0x51b601 = "Default languages missing.";
  if (App.isDevMode()) {
    App.fail(_0x51b601);
  } else {
    App.crash(_0x51b601);
  }
};
Lang.imgFolder = function (_0x362fd1, _0x573ee1) {
  var _0x4921f0 = Utils.join(_0x362fd1, _0x573ee1);
  var _0x74c153 = ConfigManager.language;
  var _0x582c92 = this.property(_0x74c153, "imageLUT", {});
  if (_0x582c92.hasOwnProperty(_0x4921f0)) {
    return _0x582c92[_0x4921f0];
  }
  return _0x362fd1;
};
Lang.imgMapping = function (_0x1a4eb6) {
  var _0x26714c = this.data[_0x1a4eb6];
  var _0x1a42d0 = Utils.dirname(this.list[_0x1a4eb6]);
  var _0x15b3c9 = Utils.join(_0x1a42d0, "img");
  if (!Utils.exists(_0x15b3c9)) {
    App.info("No translated images: " + _0x1a42d0);
    return;
  }
  for (var _0x31f81d of Utils.findFiles(_0x15b3c9, [".png"])) {
    var _0x4cb621 = Utils.relative(_0x1a42d0, _0x31f81d);
    var _0x2f04d4 = Utils.relative(App.rootPath(), _0x1a42d0);
    var _0x2bf1e3 = Utils.join(Utils.dirname(_0x4cb621), Utils.filename(_0x4cb621));
    try {
      var _0x123fc1 = Utils.join(App.rootPath(), _0x4cb621);
      var _0x5a750c = Utils.join(_0x2f04d4, Utils.dirname(_0x4cb621));
      if (Utils.exists(_0x123fc1)) {
        _0x5a750c = _0x5a750c.replace("\\", "/");
        _0x26714c.imageLUT[_0x2bf1e3] = _0x5a750c + "/";
      }
    } catch (_0x5eb0ae) {
      App.fail("Failed to check remapping: " + _0x31f81d, _0x5eb0ae);
    }
  }
};
Lang.newData = function () {
  return {
    langName: "",
    langInfo: ["", "", ""],
    fontFace: "",
    fontSize: 0,
    sysLabel: {},
    sysMenus: {},
    labelLUT: {},
    linesLUT: {},
    imageLUT: {}
  };
};
Lang.isValid = function (_0x1b4263) {
  var _0x3bb800 = this.newData();
  if (!_0x1b4263 || !Object.keys(_0x1b4263).length) {
    App.fail("Language data missing.");
    return false;
  }
  for (var _0x4201b7 in _0x3bb800) {
    if (!(_0x4201b7 in _0x1b4263)) {
      App.fail("Missing field: " + _0x4201b7);
      return false;
    }
    if (typeof _0x1b4263[_0x4201b7] !== typeof _0x3bb800[_0x4201b7]) {
      App.fail("Mismatched type: " + _0x4201b7);
      return false;
    }
  }
  if (!_0x1b4263.langName.trim()) {
    App.fail("Missing langName.");
    return false;
  }
  if (_0x1b4263.langInfo.length < 3) {
    App.fail("Missing lines in langInfo.");
    return false;
  }
  if (!_0x1b4263.fontFace.trim()) {
    App.fail("Missing fontFace.");
    return false;
  }
  if (_0x1b4263.fontSize < 1) {
    App.fail("fontSize < 1.");
    return false;
  }
  for (var _0x4201b7 of ["sysLabel", "sysMenus", "labelLUT", "linesLUT"]) {
    if (!Object.keys(_0x1b4263[_0x4201b7])) {
      App.fail(_0x4201b7 + " empty.");
      return false;
    }
  }
  return true;
};
Lang.loadLOC = function (_0x2131c0) {
  var _0x5a805f = {};
  var _0x107325 = Buffer.byteLength(SIGNATURE, "utf8");
  try {
    _0x5a805f = Utils.readFile(_0x2131c0);
    _0x5a805f = _0x5a805f.slice(_0x107325 + 4, _0x5a805f.length + 4);
    try {
      _0x5a805f = JSON.parse(_0x5a805f.toString("utf8"));
      ;
    } catch (_0x139a8b) {
      App.fail("Error parsing file: " + _0x2131c0, _0x139a8b);
    }
  } catch (_0x3abe16) {
    App.fail("Error reading file: " + _0x2131c0, _0x3abe16);
  }
  _0x5a805f.imageLUT = {};
  return _0x5a805f;
};
Lang.loadTXT = function (_0x408c1a) {
  var _0x42fb74 = "";
  try {
    _0x42fb74 = Utils.readFile(_0x408c1a, "utf8");
  } catch (_0x16746f) {
    App.fail("Error reading file: " + _0x408c1a, _0x16746f);
    return {};
  }
  var _0x592971 = this.newData();
  var _0x231a6f = 0;
  var _0x8be7a7 = "";
  var _0x4ccd00 = [];
  var _0x307dc6 = false;
  const _0xba2851 = {
    MENUS: _0x592971.sysMenus,
    LABELS: _0x592971.sysLabel,
    ITEMS: _0x592971.labelLUT,
    SPEAKERS: _0x592971.labelLUT
  };
  for (var _0x103457 of _0x42fb74.split("\n")) {
    if (!_0x103457.trim()) {
      continue;
    }
    _0x103457 = _0x103457.replace("\r", "");
    if (_0x103457.startsWith("[")) {
      if (_0x103457.toUpperCase() === "[CHOICES]") {
        _0x307dc6 = true;
        continue;
      }
      _0x231a6f = 0;
      _0x8be7a7 = _0x103457.replace("[", "").replace("]", "");
      continue;
    }
    if (!_0x8be7a7) {
      continue;
    }
    _0x231a6f += 1;
    var _0x1943a4 = _0x8be7a7.trim().toUpperCase();
    if (_0x1943a4 === "LANGUAGE") {
      _0x592971.langName = _0x103457;
      _0x8be7a7 = "";
    } else if (_0x1943a4 === "FONT") {
      var _0x549de8 = _0x103457.split(":");
      if (_0x549de8.length > 1) {
        _0x103457 = _0x549de8[1].trim();
      }
      if (_0x231a6f === 1) {
        _0x592971.fontFace = _0x103457;
      } else {
        _0x592971.fontSize = parseInt(_0x103457);
        _0x8be7a7 = "";
      }
    } else if (_0x1943a4 === "CREDITS") {
      var _0x549de8 = _0x103457.split(":");
      if (_0x549de8.length > 1) {
        _0x103457 = _0x549de8[1].trim();
      }
      _0x592971.langInfo[_0x231a6f - 1] = _0x103457;
      if (_0x231a6f >= 3) {
        _0x8be7a7 = "";
      }
    } else if (_0x1943a4 in _0xba2851) {
      if (_0x103457.includes(":")) {
        var [_0x5c81b7, _0x1aa0d6] = _0x103457.split(":");
        _0x5c81b7 = _0x5c81b7.trim();
        _0x1aa0d6 = _0x1aa0d6.trim();
        if (_0x5c81b7.startsWith("#")) {
          _0x5c81b7 = _0x5c81b7.slice(1);
        }
        _0xba2851[_0x1943a4][_0x5c81b7] = _0x1aa0d6;
      }
    } else if (_0x103457.startsWith("#")) {
      var _0x3dd070 = ":";
      if (_0x103457.includes("(")) {
        _0x3dd070 = "(";
        _0x307dc6 = false;
      }
      var _0x399b05 = _0x103457.split(_0x3dd070);
      if (_0x399b05.length < 2) {
        App.fail("Line is missing parts.\nLine: " + _0x103457 + "\nFile: " + _0x408c1a);
        return {};
      }
      var _0x5c81b7 = _0x399b05[0].trim().slice(1);
      var _0x1aa0d6 = _0x399b05[1].startsWith(" ") ? _0x399b05[1].slice(1) : _0x399b05[1];
      if (_0x307dc6) {
        _0x592971.labelLUT[_0x5c81b7] = _0x1aa0d6;
      } else {
        _0x4ccd00 = [];
        _0x592971.linesLUT[_0x5c81b7] = _0x4ccd00;
      }
    } else if (_0x103457.startsWith(":")) {
      if (_0x307dc6) {
        App.fail("Line content mismatch.\nLine: " + _0x103457 + "\nFile: " + _0x408c1a);
        return {};
      }
      _0x103457 = _0x103457.slice(1);
      if (_0x103457.startsWith(" ")) {
        _0x103457 = _0x103457.slice(1);
      }
      _0x4ccd00.push(_0x103457);
    }
  }
  return _0x592971;
};
const CSV_BLOCKS = {
  MENUS: 2,
  ITEMS: 3,
  LABELS: 3,
  SECTION: 4,
  "CREDIT 1": 3,
  SPEAKERS: 3,
  LANGUAGE: 3,
  DESCRIPTIONS: 4
};
const SECTION_HEADER = ["ID", "Source", "English", "Translation"];
Lang.is_header = function (_0x86e85e) {
  const _0x1eb4ca = SECTION_HEADER.map(_0x2bd042 => _0x2bd042.toUpperCase());
  const _0x4bb6f5 = _0x86e85e.map(_0x8e0f71 => _0x8e0f71.trim().toUpperCase());
  return JSON.stringify(_0x1eb4ca) === JSON.stringify(_0x4bb6f5);
};
Lang.new_block = function (_0x34e476, _0x28a9a6, _0x2e2169) {
  if (!Object.keys(CSV_BLOCKS).includes(_0x34e476)) {
    return false;
  }
  if (_0x34e476 === "LANGUAGE") {
    return _0x2e2169.length >= CSV_BLOCKS[_0x34e476] && (_0x2e2169[1].trim() === "Font File" || _0x2e2169[2].trim() === "Font Size" || _0x2e2169[2].trim() !== "");
  }
  if (_0x34e476 === "ITEMS") {
    return _0x2e2169.length >= CSV_BLOCKS[_0x34e476] && (_0x2e2169[1].trim() === "English" || _0x2e2169[2].trim() === "Translation" || _0x2e2169[2].trim() !== "");
  }
  return true;
};
Lang.loadCSV = function (_0x182bdd) {
  let _0x3e7a79 = this.newData();
  let _0x223488 = "";
  try {
    _0x223488 = Utils.readFile(_0x182bdd, "utf8");
  } catch (_0x3e548) {
    App.fail("Error reading file: " + _0x182bdd, _0x3e548);
    return {};
  }
  let _0xe3cdb5 = "";
  let _0x43e5f7 = "";
  let _0x4a4bdd = [];
  let _0x2d4a46 = false;
  let _0x38c457 = false;
  for (let _0x22db6d of _0x223488.split("\n")) {
    _0x22db6d = _0x22db6d.trim();
    if (!_0x22db6d) {
      continue;
    }
    let _0x1426f9 = [];
    let _0x3328e9 = "";
    let _0x4115b0 = 0;
    let _0x43792b = 0;
    let _0x50f7b1 = false;
    let _0x3a21d7 = _0x22db6d.length;
    while (_0x4115b0 < _0x3a21d7) {
      let _0x38d11a = _0x22db6d[_0x4115b0];
      if (!_0x50f7b1 && _0x38d11a === "\"") {
        _0x50f7b1 = true;
      } else if (_0x38d11a === "“" || _0x38d11a === "”") {
        _0x3328e9 += "\"";
      } else if (_0x22db6d.substr(_0x4115b0, 2) == "\"\"") {
        _0x3328e9 += "\"";
        _0x4115b0 += 1;
      } else if (_0x50f7b1 && _0x38d11a == "\"") {
        _0x50f7b1 = false;
      } else if (!_0x50f7b1 && _0x38d11a == ",") {
        _0x1426f9.push(_0x3328e9);
        _0x3328e9 = "";
      } else {
        _0x3328e9 += _0x38d11a;
      }
      _0x4115b0 += 1;
      if (_0x4115b0 >= _0x3a21d7) {
        _0x1426f9.push(_0x3328e9);
      }
    }
    let _0x285d9f = _0x1426f9.length;
    if (_0x285d9f < 1 || _0x1426f9[0].trim() === "") {
      continue;
    }
    if (_0x285d9f < 2) {
      App.fail("CSV line missing columns.\nLine: " + _0x22db6d + "\nFile: " + _0x182bdd);
      return {};
    }
    let _0x61f64a = _0x1426f9[0].toUpperCase();
    if (!_0x61f64a.trim()) {
      App.fail("CSV first column missing.\nLine: " + _0x22db6d + "\nFile: " + _0x182bdd);
      return {};
    }
    if (this.new_block(_0x61f64a, _0xe3cdb5, _0x1426f9)) {
      _0xe3cdb5 = _0x61f64a;
      _0x2d4a46 = true;
      continue;
    }
    if (_0x2d4a46 && _0xe3cdb5 === "SECTION") {
      if (this.is_header(_0x1426f9)) {
        _0x2d4a46 = false;
      }
      continue;
    }
    if (_0x285d9f < CSV_BLOCKS[_0xe3cdb5]) {
      App.fail("CSV missing columns. Total: " + CSV_BLOCKS[_0xe3cdb5] + " Found: " + _0x285d9f + "\nLine: " + _0x22db6d + "\nFile: " + _0x182bdd);
      return {};
    }
    if (_0xe3cdb5 === "LANGUAGE") {
      _0x3e7a79.langName = _0x1426f9[0];
      _0x3e7a79.fontFace = _0x1426f9[1];
      _0x3e7a79.fontSize = parseInt(_0x1426f9[2]);
      _0xe3cdb5 = "";
    } else if (_0xe3cdb5 === "CREDIT 1") {
      _0x3e7a79.langInfo = _0x1426f9.slice(0, Math.min(3, _0x285d9f));
      _0xe3cdb5 = "";
    } else if (_0xe3cdb5 === "LABELS") {
      _0x3e7a79.sysLabel[_0x1426f9[0]] = _0x1426f9[2].trim() ? _0x1426f9[2] : _0x1426f9[1];
    } else if (_0xe3cdb5 === "MENUS") {
      _0x3e7a79.sysMenus[_0x1426f9[0]] = _0x1426f9[1].trim() ? _0x1426f9[1] : _0x1426f9[0];
    } else if (_0xe3cdb5 === "ITEMS" || _0xe3cdb5 === "SPEAKERS") {
      let _0x5de68e = _0x1426f9[0];
      let _0x1b145e = _0x1426f9[1];
      let _0x6a9d20 = _0x1426f9[2];
      if (!_0x5de68e.trim() || !_0x1b145e.trim()) {
        App.fail("Missing column data for Item.\nLine: " + _0x22db6d + "\nFile: " + _0x182bdd);
        return {};
      }
      _0x6a9d20 = _0x6a9d20.trim() ? _0x6a9d20 : _0x1b145e;
      _0x3e7a79.labelLUT[_0x5de68e] = _0x6a9d20;
    } else if (_0xe3cdb5 === "SECTION" || _0xe3cdb5 === "DESCRIPTIONS") {
      let _0x52c0e5 = _0x1426f9[0];
      let _0x5858da = _0x1426f9[1];
      let _0x35985 = _0x1426f9[2];
      let _0x52f4ea = _0x1426f9[3];
      if (!_0x52c0e5.trim() || !_0x5858da.trim()) {
        App.fail("Missing column data for Section.\nLine: " + _0x22db6d + "\nFile: " + _0x182bdd);
        return {};
      }
      _0x52f4ea = _0x52f4ea.trim() ? _0x52f4ea : _0x35985;
      if (_0x5858da.toUpperCase().includes("CHOICE")) {
        _0x3e7a79.labelLUT[_0x52c0e5] = _0x52f4ea;
      } else {
        if (_0x43e5f7 != _0x52c0e5) {
          _0x4a4bdd = [];
          _0x3e7a79.linesLUT[_0x52c0e5] = _0x4a4bdd;
          _0x43e5f7 = _0x52c0e5;
        }
        _0x4a4bdd.push(_0x52f4ea);
      }
    } else {
      App.fail("Invalid CSV parsing state.");
      return {};
    }
  }
  return _0x3e7a79;
};
const _DM_OL = DataManager.onLoad;
DataManager.onLoad = function (_0xc85443) {
  if (_0xc85443 === $dataSystem) {
    Lang.search();
    Lang.select(ConfigManager.language);
  }
  _DM_OL.call(this, _0xc85443);
};
const MAX_LINES = 2;
Game_Interpreter.prototype.prevHeader = "";
Game_Interpreter.prototype.extraLines = [];
Game_Interpreter.prototype.command101 = function () {
  if (!$gameMessage.isBusy()) {
    if (this.extraLines.length > 0) {
      $gameMessage.add(this.prevHeader);
      var _0x321439 = Math.min(this.extraLines.length, MAX_LINES);
      for (var _0x28d835 = 0; _0x28d835 < _0x321439; _0x28d835++) {
        $gameMessage.add(this.extraLines.shift());
      }
      if (this.extraLines.length < 1) {
        this._index++;
      }
      this.setWaitMode("message");
      return false;
    }
    $gameMessage.setFaceImage(this._params[0], this._params[1]);
    $gameMessage.setBackground(this._params[2]);
    $gameMessage.setPositionType(this._params[3]);
    while (this.nextEventCode() === 401) {
      this._index++;
      var _0x1b52e7 = this.currentCommand().parameters[0];
      var _0x3c14e4 = Lang.lines(Lang.label(_0x1b52e7, true));
      $gameMessage.add(_0x3c14e4.text);
      this.prevHeader = _0x3c14e4.text;
      if (_0x3c14e4.lines.length) {
        for (var _0x28d835 = 0; _0x28d835 < _0x3c14e4.lines.length; _0x28d835++) {
          if (_0x28d835 < MAX_LINES) {
            $gameMessage.add(_0x3c14e4.lines[_0x28d835]);
          } else {
            this.extraLines.push(_0x3c14e4.lines[_0x28d835]);
          }
        }
      }
      if (this.extraLines.length > 0) {
        while (this._index >= 0 && this.currentCommand().code !== 101) {
          this._index--;
        }
        this.setWaitMode("message");
        return;
      }
    }
    switch (this.nextEventCode()) {
      case 102:
        this._index++;
        this.setupChoices(this.currentCommand().parameters);
        break;
      case 103:
        this._index++;
        this.setupNumInput(this.currentCommand().parameters);
        break;
      case 104:
        this._index++;
        this.setupItemChoice(this.currentCommand().parameters);
        break;
    }
    this._index++;
    this.setWaitMode("message");
  }
  return false;
};
Game_Interpreter.prototype.setupChoices = function (_0x44e668) {
  var _0x5e439f = _0x44e668[0].clone();
  for (let _0x3c6ffb = 0; _0x3c6ffb < _0x5e439f.length; _0x3c6ffb++) {
    _0x5e439f[_0x3c6ffb] = Lang.label(_0x5e439f[_0x3c6ffb]);
  }
  var _0x16a614 = _0x44e668[1];
  var _0x1292b3 = _0x44e668.length > 2 ? _0x44e668[2] : 0;
  var _0x419185 = _0x44e668.length > 3 ? _0x44e668[3] : 2;
  var _0x229e85 = _0x44e668.length > 4 ? _0x44e668[4] : 0;
  if (_0x16a614 >= _0x5e439f.length) {
    _0x16a614 = -2;
  }
  $gameMessage.setChoices(_0x5e439f, _0x1292b3, _0x16a614);
  $gameMessage.setChoiceBackground(_0x229e85);
  $gameMessage.setChoicePositionType(_0x419185);
  $gameMessage.setChoiceCallback(function (_0x375ac0) {
    this._branch[this._indent] = _0x375ac0;
  }.bind(this));
};
Window_Base.prototype.drawItemName = function (_0x20df40, _0x20fce, _0x5c58d3, _0x1db0ae) {
  _0x1db0ae = _0x1db0ae || 312;
  if (_0x20df40) {
    var _0x16857a = Window_Base._iconWidth + 4;
    this.resetTextColor();
    this.drawIcon(_0x20df40.iconIndex, _0x20fce + 2, _0x5c58d3 + 2);
    this.drawText(Lang.label(_0x20df40.name), _0x20fce + _0x16857a, _0x5c58d3, _0x1db0ae - _0x16857a);
  }
};
Window_Help.prototype.setItem = function (_0x51fa7d) {
  if (!_0x51fa7d) {
    this.setText("");
    return;
  }
  var _0x45a09b = Lang.lines(_0x51fa7d.description);
  if (_0x45a09b.lines.length > 0) {
    this.setText(_0x45a09b.lines.join("\n"));
  } else {
    this.setText(_0x51fa7d.description);
  }
};
WebAudio.prototype._readMetaData = function (_0x4f6eaa, _0x355476, _0x3f9666) {
  for (var _0x36a747 = _0x355476; _0x36a747 < _0x355476 + _0x3f9666 - 10; _0x36a747++) {
    if (this._readFourCharacters(_0x4f6eaa, _0x36a747) === "LOOP") {
      var _0x386a1a = "";
      while (_0x4f6eaa[_0x36a747] > 0) {
        _0x386a1a += String.fromCharCode(_0x4f6eaa[_0x36a747++]);
      }
      let _0x3c2701 = _0x386a1a.match(/LOOPSTART=([0-9]+)/);
      if (_0x3c2701 && _0x3c2701.length > 1) {
        this._loopStart = parseInt(_0x3c2701[1]);
      }
      _0x3c2701 = _0x386a1a.match(/LOOPLENGTH=([0-9]+)/);
      if (_0x3c2701 && _0x3c2701.length > 1) {
        this._loopLength = parseInt(_0x3c2701[1]);
      }
      if (_0x386a1a == "LOOPSTART" || _0x386a1a == "LOOPLENGTH") {
        var _0x579171 = "";
        _0x36a747 += 16;
        while (_0x4f6eaa[_0x36a747] > 0) {
          _0x579171 += String.fromCharCode(_0x4f6eaa[_0x36a747++]);
        }
        if (_0x386a1a == "LOOPSTART") {
          this._loopStart = parseInt(_0x579171);
        } else {
          this._loopLength = parseInt(_0x579171);
        }
      }
    }
  }
};