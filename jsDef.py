# // Javascript Definitions for ThunderCOAL



# Converts a Python dictionary to a Javascript dictionary.
def JS_PyConvDict(d):
    return str(d).replace("True", "true").replace("False", "false").replace("'", '"').replace(": ", ":").replace(', "', ',"')