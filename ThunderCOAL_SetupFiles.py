from reqDef import *
from chkDef import *
from patchDef import *
from steamDef import *
from messageDef import *
from subprocess import run
from zipfile import ZipFile
from jsDef import JS_PyConvDict
from os import remove, makedirs, path
from shutil import copy2, copytree, rmtree
from miscDef import MISC_BruteforceRead, MISC_StringSeek



PluginThunder = {
"name":"ThunderCOAL",
"status":True,
"description":"Main Game Code for ThunderCOAL.",
"parameters":{}
}

PluginThunderJS = JS_PyConvDict(PluginThunder) + ",\n"

GrimoireFetchCode = ["deno", "run", "--allow-read", "--allow-write=clean.js", "--allow-env", "--allow-sys", "https://www.codeberg.org/basil/grimoire/raw/branch/main/deobfuscate/main.ts"]
GrimoireFetchAssets = ["deno", "run", "--allow-read", "--allow-write=out/", "--allow-env", "--allow-sys", "https://www.codeberg.org/basil/grimoire/raw/branch/main/decrypt/main.ts", "decrypt"]

NWJSFetchURL = "https://dl.nwjs.io/v0.29.0/nwjs-sdk-v0.29.0-win-ia32.zip"



GameDir = input("Game directory: ")
GameDir = GameDir.strip(" ").rstrip("/").replace("\\", "/")

if path.isfile("clean.js") != True:
    SetupCode = run(GrimoireFetchCode)
    if SetupCode.returncode != 0:
        MSG_MessageHandler("EnvSetup")
    MSG_MessageHandler("EnvSetup", 1)

print("Copying grimoire output to /www/js/plugins/...")
copy2("clean.js", GameDir + "/www/js/plugins/ThunderCOALTemp.js")
print("Done!")

if path.isdir("out") != True:
    while(True):
        AssetOpt = input("Would you like to run the game using decrypted assets? [Y/N]\n")
        if AssetOpt.upper() in ["Y", "N"]:
            break
    if AssetOpt.upper() == "Y":
        print("Decrypting assets, this may take some time...")
        SetupAssets = run(GrimoireFetchCode)
        if SetupAssets.returncode != 0:
            MSG_MessageHandler("EnvSetup")
        MSG_MessageHandler("EnvSetup", 1)
        print("Copying decrypted assets, this may take some time...")
        copytree("out", GameDir + "/www/", dirs_exist_ok=True)

print("Updating plugin.js...")
with open(GameDir + "/www/js/plugins.js", "r+") as PluginFile:
    PluginFileList = PluginFile.readlines()
    if PluginFileList[23] != PluginThunderJS:
       PluginFileList.insert(23, PluginThunderJS)
       PluginFile.seek(0)
       PluginFile.write("".join(PluginFileList))
    PluginFile.close()

print("Done!")

CHK_VerifyFile((GameDir + "/www/js/plugins.js"), "")

print("Removing Steam/Greenworks DRM...")
with open(GameDir + "/www/js/plugins/ThunderCOALTemp.js", "r+") as PluginThunderFileI:
    with open(GameDir + "/www/js/plugins/ThunderCOAL.js", "w") as PluginThunderFileO:
        
        MISC_StringSeek(PluginThunderFileI, 'const VERSION = "')
        PluginThunderFileVer = MISC_BruteforceRead(PluginThunderFileI)
        PluginThunderFilePatch = DRM_CodePatch(PluginThunderFileI, PluginThunderFileVer)
    
        PluginThunderFileO.seek(0)
        PluginThunderFileO.write(PluginThunderFilePatch)
        PluginThunderFileO.close()

    PluginThunderFileI.close()

remove(GameDir + "/www/js/plugins/ThunderCOALTemp.js")
print("Done!")

if CHK_VerifyFile((GameDir + "/www/js/plugins/ThunderCOAL.js"), PluginThunderFileVer.replace(".", "")) == True:
    print("Successfully verified all files!")

print("Replacing core NW.js components with NW.js SDK components...")
if path.isdir("assets") != True:
    makedirs("assets")

    REQ_RetrFile(NWJSFetchURL, "assets")
    NWJSExec = ZipFile("assets/" + basename(NWJSFetchURL))
    print("Extracting file...")
    NWJSExec.extractall("assets")
    print("Done!")
    NWJSExecFolder = basename(NWJSFetchURL).replace(".zip", "")

    print("Replacing executable...")
    copy2("assets/" + NWJSExecFolder + "/nw.exe", GameDir + "/Game.exe")
    print("Done!")
    print("Replacing other files...")
    copytree("assets/" + NWJSExecFolder, GameDir, dirs_exist_ok=True)
    print("Done!")

    print("Deleting unused files/folders...")

    rmtree(GameDir + "/pnacl")

    NWJSUnusedFiles = ["chromedriver.exe", "nacl_irt_x86_32.nexe", "nacl_irt_x86_64.nexe", "nacl64.exe", "nw.exe", "nwjc.exe", "payload.exe"]

    i = 0
    while(True):
        remove(GameDir + "/" + NWJSUnusedFiles[i])
        i += 1
        if i == len(NWJSUnusedFiles):
            break
    
    print("Done!")
    
    exit(0)