# // Exception & Success Message Definitions for ThunderCOAL



# - All dictionaries that contain all Exception & Success Messages.

# The Dictionary that handles all Exception Messages.
MSG_ExceptionMessage = {

"EnvSetupFail":[
    "Environment setup failed."
],

"GetVersionDRMFail":[
    "Couldn't process version!",
    "There could be many causes for this:",
    "",
    "- Version hasn't been implemented yet (most likely).",
    "- Version is invalid.",
    "- Version is from itch.io and doesn't contain any DRM."
],

"VerifyFile0Fail":[
    "Could not verify existence of file!",
    "There could be many causes for this:",
    "",
    "- Directory is read-only (most likely).",
    "- Directory ran out of disk space."
],

"VerifyFile1Fail":[
    "Could not verify CRC-32 of file!",
    "There could be many causes for this:",
    "",
    "- File was patched with a non-vanilla file (most likely).",
    "- CRC-32 hasn't been added to database yet."
],

"SteamGetLangFail":[
    "String is not a valid BCP-47 Code."
]

}

# The Dictionary that handles all Success Messages.
MSG_SuccessMessage = {

"EnvSetupPass":[
    "Environment setup passed."
],

"GetVersionDRMPass":[
    "Successfully processed version!"
],

"VerifyFile0Pass":[
    "Successfully verified existence of file!"
],

"VerifyFile1Pass":[
    "Successfully verified CRC-32 of file!"
],

"SteamGetLangPass":[
    "String is a valid BCP-47 Code!"
]

}



# Exception/Success Message Handler.
def MSG_MessageHandler(msg, flag=0):

    handlerKey = MSG_ExceptionMessage
    handlerMsg = (msg + "Fail")

    if flag == 1:
        handlerKey = MSG_SuccessMessage
        handlerMsg = (msg + "Pass")
        return print("\n".join(handlerKey[handlerMsg]))
    
    raise Exception("\n".join(handlerKey[handlerMsg]))