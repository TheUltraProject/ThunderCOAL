# ThunderCOAL

An advanced modding toolkit for The Coffin of Andy and Leyley, written in Python (and Batch).

Tested with every single version that contains DRM (v2.0.0 - v2.0.9), to ensure compatibility.



# Setup

## Install Dependencies

### Python3

To install Python3 for your respective platform, install the appropriate package for your platform [at Python's official website](https://www.python.org/downloads/release/python-3121).

### Deno

To install Deno for your respective platform, follow the instructions provided [at Deno's official website](https://docs.deno.com/runtime/manual/getting_started/installation).

### The Coffin of Andy and Leyley

*You will need to have purchased a copy of The Coffin of Andy and Leyley from Steam (not from itch.io, as it's not supported yet).*

After you've obtained that, you will need to extract the game to ThunderCOAL's root directory.

To do that:

- Launch Steam (Necessary!)
- Select "Settings".
- Select the "Steam" option in the top-left corner of the menu.
- Select "Storage".
- Select the option below the "Storage" section that specifies your current drive.
- At the dropdown menu, select the drive where you have The Coffin of Andy and Leyley installed, and wait for it to load.
- Wait until "The Coffin of Andy and Leyley" is present under the "Games" section.
- Select the icon with 3 Dots, right above the "Games" section.
- At the dropdown menu, select "Browse Folder".
- Wait until the File Explorer menu appears (you should see a list of folders that say "`common`", "`downloading`", "`shadercache`", etc).
- Go into `/common/`, and copy the `/The Coffin of Andy and Leyley/` folder.
- Open another File Explorer window, and extract ThunderCOAL to a directory somewhere (preferably your "`Downloads`" folder).
- Once you've installed ThunderCOAL, go into the directory where ThunderCOAL is present, then go into `/ThunderCOAL/`.
- Paste the folder that you just copied; you have now successfully copied the main game code of The Coffin of Andy and Leyley to ThunderCOAL.
- (Optional) If you'd like, you can rename the `/The Coffin of Andy and Leyley/` folder to something different, as grimoire & ThunderCOAL both accept custom folder names.

After that, you can now run the initial setup for ThunderCOAL.

## How to Setup

- Run `ThunderCOAL_Setup.bat`.

This will initialize ThunderCOAL.

If you want to run it via CMD, simply run `python ThunderCOAL_SetupFiles.py` in the command prompt.