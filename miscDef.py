# // Miscellaneous Definitions for ThunderCOAL



# Seeks to a value in a file where a given string is present, plus an optional "seek" value.
def MISC_StringSeek(f, string, seek=0):
    
    fData = f.read()
    return f.seek((len(string) + seek) + fData.rfind(string))

# Bruteforce reads through a string in a file.
def MISC_BruteforceRead(f):

    fList = []
    fPos = f.tell()

    while(True):
        
        fLetter = f.read(1)
        if fLetter == '"':
            break
        fList.append(fLetter)
        fPos += 1
        f.seek(fPos)
    
    fOutput = ''.join(fList)
    return fOutput

# Format a string as a string. Useful for injecting strings into JavaScript code.
def MISC_StringFormat(string):
    return '"' + string + '"'