# // Request Definitions for ThunderCOAL

from requests import get
from messageDef import *
from ntpath import basename



def REQ_RetrFile(url, dirstr):

    print("Fetching file from URL...")
    retrSignal = get(url)
    
    if retrSignal.status_code != 200:
        MSG_MessageHandler("ReqRetrieveUrl")
    
    print("Done!")
    
    print("Writing file to disk...")

    with open(dirstr.strip(" ").rstrip("/").replace("\\", "/") + ("/" if dirstr != "" else "")  + basename(url), 'wb') as retrIn:
        retrIn.write(retrSignal.content)
        retrIn.close()

    MSG_MessageHandler("ReqRetrieveUrl", 1)

    return print("Done!")